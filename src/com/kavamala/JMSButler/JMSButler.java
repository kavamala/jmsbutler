/**
 * 
 */
package com.kavamala.JMSButler;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Properties;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.jms.JMSException;

import com.tibco.tibjms.admin.TibjmsAdmin;
import com.tibco.tibjms.admin.TibjmsAdminException;

/**
 * @author Daniele Galluccio
 *
 * v.1.5 
 * Fixed error with create brigde and complex selector (string was not getting updated)
 * 
 * v.1.6 
 * Fixed corner case: promotion of dynamic queues
 * 
 */
public final class JMSButler {
	
	TibjmsAdmin ta;
	
	final String version = Msg.get("version.number"); //$NON-NLS-1$
	public static int errStatus = -1;
	
	String customer = null;
	Date endOfLicense = null;
	private ArrayList<String> errors = new ArrayList<String>();

	String serverUrl=Msg.get("EB.1"); //$NON-NLS-1$
	String userName=Msg.get("EB.2"); //$NON-NLS-1$
	String password=Msg.get("EB.3");	 //$NON-NLS-1$
	private String commandFile = Msg.get("EB.4"); //$NON-NLS-1$
	String licenseFile = Msg.get("JB.default.license"); //$NON-NLS-1$
	private int allowedCmds = 10;
	
	private String p2="AOHytu2QqA";
	
	
    // SSL options
    boolean     ssl_trace                = true;
    boolean     ssl_debug_trace          = true;
    Vector<Object>      ssl_trusted              = new Vector<Object>();
    String      ssl_hostname             = null;
    String      ssl_identity             = null;
    String      ssl_key                  = null;
    String      ssl_password             = null;
    String      ssl_vendor               = null;
    String      ssl_ciphers              = null;
    boolean     disable_verify_host_name = true;
    boolean     disable_verify_host      = true;
	
	
	/**
	 * @throws TibjmsAdminException 
	 * 
	 */
	public JMSButler(String[] args) throws Exception {
		Properties sysProp = EMSUtils.parseArgs(args);
		Hashtable<Object, Object> sslProps = null;
		if (!sysProp.isEmpty()) {
			if (sysProp.getProperty(Msg.get("EB.6"))!=null) { //$NON-NLS-1$
				loadCredentials(sysProp.getProperty(Msg.get("EB.7"))); //$NON-NLS-1$
			} else {
				serverUrl = sysProp.getProperty(Msg.get("EB.8"), Msg.get("EB.9")); //$NON-NLS-1$ //$NON-NLS-2$
				userName= sysProp.getProperty(Msg.get("EB.10"), Msg.get("EB.11")); //$NON-NLS-1$ //$NON-NLS-2$
				password= sysProp.getProperty(Msg.get("EB.12"), Msg.get("EB.13"));				 //$NON-NLS-1$ //$NON-NLS-2$
			}
			setCommandFile(sysProp.getProperty(Msg.get("EB.14"), Msg.get("EB.15"))); //$NON-NLS-1$ //$NON-NLS-2$
			licenseFile= sysProp.getProperty(Msg.get("EB.16"), Msg.get("EB.17")); //$NON-NLS-1$ //$NON-NLS-2$
		}
		if (serverUrl.startsWith("ssl://")) {
			sslProps = InitSSL();
			ta = new TibjmsAdmin(serverUrl, userName, password, sslProps);
		} else {
			ta = new TibjmsAdmin(serverUrl, userName, password);			
		}
		
		p_chkForTrialVersion(licenseFile);

	}

	private void p_chkForTrialVersion(String filename) throws JMSButlerException {
		Scanner scanner = null;
		
	    try {
	    	scanner = new Scanner(new FileReader(filename));
		    final String passPhrase   = p_getPassphrase();
		    GenUtils desEncrypter = new GenUtils(passPhrase);
	    	String t1 = scanner.nextLine();
	    	customer = desEncrypter.decrypt(t1);
	    	
	    	String date = desEncrypter.decrypt(scanner.nextLine());

	    	String tmpNum = desEncrypter.decrypt(scanner.nextLine());	    	
	    	
	    	DateFormat formatter = new SimpleDateFormat(Msg.get("EB.62"));  //$NON-NLS-1$
	    	endOfLicense = formatter.parse(date);
	    	Date today = new Date();
	    	
	    	if (!endOfLicense.before(today)) {
	    		allowedCmds = Integer.parseInt(tmpNum);
	    	} else {
	    		System.err.println("License is expired, switching back to trial mode.");
	    	}
	    	
	    } catch (FileNotFoundException e) {
	    	// if license file is missing enters trial mode
	    	//System.err.println("License file not found, switching to trial mode.");
	    } catch (NumberFormatException e) {
	    	// if license file is missing enters trial mode
	    	//System.err.println("License file corrupted, switching to trial mode.");
		} catch (ParseException e) {
			//e.printStackTrace();
			throw new JMSButlerException("Error while reading License File", e);
		}
	    finally {
	    	if (scanner != null)
	    		{ scanner.close(); }
	    }	

	}

	public JMSButler() throws TibjmsAdminException {
		ta = new TibjmsAdmin(serverUrl, userName, password);
	}	
	
	public JMSButler(int dummy) throws TibjmsAdminException {
		ta = null;
	}		
	
	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {

		int status = -1;
		JMSButler eb = null;
		try {
			eb = new JMSButler(args);
		} catch (TibjmsAdminException e) {
			System.out.println(Msg.get("EB.18")); //$NON-NLS-1$
			System.out.println(e.getLocalizedMessage());
			e.printStackTrace();
			System.exit(status);
		}
		
		eb.printInfo();

		try {
			eb.p_parseFile(eb.getCommandFile());
		} catch (Exception e) {
			System.out.println(Msg.get("EB.19")); //$NON-NLS-1$
			System.out.println(e.getLocalizedMessage());
			e.printStackTrace();
			System.exit(status);
		}
		status = eb.getErrors().size();
		System.exit(status);
	}
	
	

	public void printInfo() throws Exception {
		printLicenseInfo(licenseFile);
		// Connecting to: tcp://daniele-614cdae:7222 as admin
		System.out.println(Msg.get("EB.20")+this.serverUrl + Msg.get("EB.21") + this.userName);	
	}


	public void printLicenseInfo(final String filename) throws Exception {
	
//		try {
			boolean isLicenseValid = p_checkLicense(filename);
			System.out.println(Msg.get("EB.22") + version);
			if (isLicenseValid) {
				// This copy of JMSButler is licensed to: THIS IS A DEMO LICENCE
				// License is valid until: Sun Jan 01 00:00:00 GMT 2012
				System.out.println(Msg.get("EB.23") + customer); //$NON-NLS-1$
				System.out.println(Msg.get("EB.24") + endOfLicense); //$NON-NLS-1$
			} else {
				if (customer != null) {
					System.out.println(Msg.get("EB.25") + customer); 
				} else {
					System.out.println("Running in trial Mode\nMaximum " + allowedCmds + " commands will be performed"); 
				}
				//System.out.println(Msg.get("EB.26"));			 //$NON-NLS-1$
			}
			System.out.println(EMSConstants.dashLine);
//		} catch (Exception e) {
//			//e.printStackTrace();
//			System.out.println(e.getLocalizedMessage());
//			System.out.println(e.toString());			
//		}
	}

	
	public void parseCommand(String line) throws Exception {
		p_parseCommand(line);
	}

	
	
	/**
	 * @param args
	 */
	private void p_parseCommand(String line) throws JMSButlerException {

		StringTokenizer st = new StringTokenizer(line);	
		
		//debug
		if (!(line.trim().equals(""))) {
			System.out.println(Msg.get("EB.27")+line); //$NON-NLS-1$
		}
		String command;
		
		try {
			if (line.trim().startsWith(Msg.get("EB.28"))) { //$NON-NLS-1$
				// ignore comment lines
			}
			else {
				ArrayList<String> unparsedCommands = new ArrayList<String>();
				while (st.hasMoreElements()) {
					unparsedCommands.add(st.nextToken());
				}
				

				
				boolean validCommand = false;
				// test number of commands in line.
				switch (unparsedCommands.size()) {
				case 0: 
					// ignore empty lines
					validCommand = true;
					break;
				case 1:
					//commit, rotatelog, shutdown, updatecrl, whoami
					command = unparsedCommands.get(0);
					if (command.equalsIgnoreCase(Msg.get("EB.29"))) { //commit
						ta.saveConfiguration();
						validCommand = true;
					} else if (command.equalsIgnoreCase(Msg.get("EB.30"))) { //rotatelog
						ta.rotateLog();
						validCommand = true;
					} else if (command.equalsIgnoreCase(Msg.get("EB.31"))) { //shutdown
						ta.shutdown(); //TODO allow shutdown ??
						validCommand = true;
					} else if (command.equalsIgnoreCase(Msg.get("EB.32"))) { //updatecrl
						ta.updateCRL();
						validCommand = true;
					} else if (command.equalsIgnoreCase(Msg.get("EB.33"))) { //whoami
						// NB. whoami not implemented in the API
						System.out.println(userName);
						validCommand = true;
					}
					break;
				case 2:
					// autocommit [on|off], echo [on|off], time [on | off], timeout [seconds]
					command = unparsedCommands.get(0);
					if (command.equalsIgnoreCase("show")) {
						validCommand = true;
						// skip show commands
						System.out.println("show command not supported yet, skipping");
						break;
					}
					break;
				default:
					// standard command (more than 2 arguments)
					command = unparsedCommands.get(0);
					String objectType = unparsedCommands.get(1);
					//String objectName = unparsedCommands.get(2);
					validCommand = true;
					if (command.equalsIgnoreCase("connect")) {
						// skip connect commands
						System.out.println("Connect command not supported as redundant from properties file, skipping");
						break;
					}
					if (command.equalsIgnoreCase("show")) {
						// skip show commands
						System.out.println("show command not supported yet, skipping");
						break;
					}
					if (objectType.equalsIgnoreCase(Msg.get("EB.34"))) { //queue
						EMSQueueManager eqm = new EMSQueueManager(line, ta);
						if (command.equalsIgnoreCase(Msg.get("EB.35")) || command.equalsIgnoreCase(Msg.get("EB.36"))) { //$NON-NLS-1$ //$NON-NLS-2$
							eqm.createOrUpdateQueue();
						}
						if (command.equalsIgnoreCase(Msg.get("EB.37"))) { //delete
							eqm.deleteQueue();
						}
						if (command.equalsIgnoreCase("grant") || command.equalsIgnoreCase("revoke")) {
							EMSPermissionManager epm = new EMSPermissionManager(line, ta); 
								epm.setPermissions();
						}
					} else if (objectType.equalsIgnoreCase(Msg.get("EB.39"))) { //topic
						EMSTopicManager eqm = new EMSTopicManager(line, ta);
						if (command.equalsIgnoreCase(Msg.get("EB.40")) || command.equalsIgnoreCase(Msg.get("EB.41"))) { //$NON-NLS-1$ //$NON-NLS-2$
							eqm.createOrUpdateTopic();
						}
						if (command.equalsIgnoreCase(Msg.get("EB.42"))) { //$NON-NLS-1$
							eqm.deleteTopic();
						}
						if (command.equalsIgnoreCase(Msg.get("EB.43"))) { //$NON-NLS-1$
							EMSPermissionManager epm = new EMSPermissionManager(line, ta);
							epm.setPermissions();
						}
					} else if (objectType.equalsIgnoreCase(Msg.get("EB.44"))) { //bridge
						EMSBridgeManager ebm = new EMSBridgeManager(line, ta);
						if (command.equalsIgnoreCase(Msg.get("EB.45"))) { //$NON-NLS-1$
							ebm.createOrUpdateBridge();
						}
						if (command.equalsIgnoreCase(Msg.get("EB.46"))) { //$NON-NLS-1$
							ebm.deleteBridge();
						}
					} else if (objectType.equalsIgnoreCase(Msg.get("EB.47"))) { //group
						EMSGroupManager ebm = new EMSGroupManager(line, ta);
						if (command.equalsIgnoreCase(Msg.get("EB.48"))) { //create
							ebm.createOrUpdateGroup();
						}
						if (command.equalsIgnoreCase(Msg.get("EB.49"))) { //delete
							ebm.deleteGroup();
						}
					} else if (objectType.equalsIgnoreCase(Msg.get("JB.grantCmd")) || objectType.equalsIgnoreCase(Msg.get("JB.revokeCmd"))) { //$NON-NLS-1$
						EMSPermissionManager epm = new EMSPermissionManager(line, ta); 
						epm.setPermissions();
					} else if (objectType.equalsIgnoreCase(Msg.get("EB.51"))) { //$NON-NLS-1$
						EMSUserManager eum = new EMSUserManager(line, ta);
						if (command.equalsIgnoreCase(Msg.get("EB.52"))) { //$NON-NLS-1$
							eum.createOrUpdateUser();
						}
						if (command.equalsIgnoreCase(Msg.get("EB.53"))) { //$NON-NLS-1$
							eum.deleteUser();
						}
						// TODO write javadoc
					} else if (objectType.equalsIgnoreCase("all")) { //all
						if (command.equalsIgnoreCase("delete")) {
							if (unparsedCommands.get(2).equalsIgnoreCase("queues")) {
								EMSQueueManager eqm = new EMSQueueManager(line, ta);
								eqm.deleteAllQueues();
							} else if (unparsedCommands.get(2).equalsIgnoreCase("topics")) {
								EMSTopicManager etm = new EMSTopicManager(line, ta);
								etm.deleteAllTopics();
							} else if (unparsedCommands.get(2).equalsIgnoreCase("users")) {
								EMSUserManager eum = new EMSUserManager(line, ta);
								eum.deleteAllUsers();
							} else if (unparsedCommands.get(2).equalsIgnoreCase("groups")) {
								EMSGroupManager egm = new EMSGroupManager(line, ta);
								egm.deleteAllGroups();
							} else if (unparsedCommands.get(2).equalsIgnoreCase("durables")) {
								throw new JMSButlerException("unsupported command");
							}
						} else {
							validCommand = false;
						}
					} else {
						//System.out.println("!! Command not supported:\n-> "+line);
						validCommand = false;
						//throw new JMSButlerException("!! Command not supported: "+line);
					} 
					break;
				}
				if (!validCommand) {
					System.err.println(Msg.get("EB.54")+line); //$NON-NLS-1$
					throw new JMSButlerException(Msg.get("EB.55")+line); //$NON-NLS-1$
				}
			}
		} catch (TibjmsAdminException e) {
			//e.printStackTrace();
//			System.err.println("--- error: " + e.getMessage());
//			System.err.flush();
			throw new JMSButlerException(Msg.get("EB.56") + line, e); //$NON-NLS-1$
		} catch (Exception e) {
			//e.printStackTrace();
//			System.err.println("--- error: " + e.getMessage());
//			System.err.flush();
			throw new JMSButlerException(Msg.get("EB.57") + line,e); //$NON-NLS-1$
		}
	}


	
	public void parseFile(String filename) throws Exception {
		p_parseFile(filename);
	}

	
	private void p_parseFile(String filename) throws Exception {
		
//		boolean ok = this.p_checkLicense(licenseFile);
//		if (!ok) {
//			throw new JMSButlerException(Msg.get("EB.58")); //$NON-NLS-1$
//		}
		
		int performedCmds = 1;
		Scanner scanner = new Scanner(new FileReader(filename));
	    try {
	      while ( scanner.hasNextLine() ){
	    	String line = scanner.nextLine();
	    	try {
	
	        	if (!(line.trim().startsWith("#") || line.equals("")|| line.equals("\n"))) {
		    		if (performedCmds++ > allowedCmds) {
		    			System.out.println(Msg.get("EB.27")+line);
		    			System.out.println("Maximum commands in trial mode reached, skipping execution.");
		    			continue;
		    		} else {
		    			p_parseCommand(line);
		    		}
		    	} else {
		    		p_parseCommand(line);
		    	}
		    		
			} catch (JMSButlerException x) {
				// Add error to log and go on with the other commands
				getErrors().add(line);
				System.err.println(x.getMessage());
			} // end try/catch
	      } // end while
	    } catch (Exception e) {
			e.printStackTrace();
		}
	    finally {
	      scanner.close();	      
	    } // end finally	

	  System.out.println(EMSConstants.dashLine);
      if (getErrors().size() != 0) {
    	  int i=1;
    	  System.out.println(Msg.get("EB.59")); //$NON-NLS-1$
	      for (Iterator<String> err = getErrors().iterator(); err.hasNext();) {
				String error = err.next();
				System.out.println(i + ": " + error);
				i++;
		  }
      } else {
    	  System.out.println(Msg.get("EB.60")); //$NON-NLS-1$
      }
	    
	} // end p_parseFile

	public boolean checkLicense(String filename) throws JMSButlerException {
		return p_checkLicense(filename);
	}
	
	private boolean p_checkLicense(String filename) throws JMSButlerException {
		Scanner scanner = null;
		boolean isLicenseValid = false;
		
	    try {
	    	scanner = new Scanner(new FileReader(filename));
		    final String passPhrase   = p_getPassphrase();
		    GenUtils desEncrypter = new GenUtils(passPhrase);
	    	String t1 = scanner.nextLine();
	    	customer = desEncrypter.decrypt(t1);	
	    	String date = desEncrypter.decrypt(scanner.nextLine());
	    	
	    	DateFormat formatter = new SimpleDateFormat(Msg.get("EB.62"));  //$NON-NLS-1$
	    	endOfLicense = formatter.parse(date);
	    	Date today = new Date();
	    	if (!endOfLicense.before(today)) {
	    		isLicenseValid = true;
	    	}
	    } catch (FileNotFoundException e) {
	    	//e.printStackTrace();
	    	// if license file is missing enters trial mode
	    	// throw new JMSButlerException("Missing License File", e);
	    	//System.err.println("License file not found, switching to trial mode.");
		} catch (ParseException e) {
			//e.printStackTrace();
			throw new JMSButlerException("Error while reading License File", e);
		}
	    finally {
	    	if (scanner != null)
	    		{ scanner.close(); }
	    }	
	    return isLicenseValid;
	    
	}

	public void loadCredentials(final String filename) throws Exception {
		Scanner scanner = null;
	    try {
	    	//scanner = new Scanner(new FileReader(filename));
		    Properties c = new Properties();
		    c.load(new FileInputStream(filename));

		    serverUrl=c.getProperty(Msg.get("EB.63")); //$NON-NLS-1$
		    userName=c.getProperty(Msg.get("EB.64"), Msg.get("EB.65")); //$NON-NLS-1$ //$NON-NLS-2$
		    password=c.getProperty(Msg.get("EB.66"), Msg.get("EB.67")); //$NON-NLS-1$ //$NON-NLS-2$
		    if (password.startsWith(Msg.get("EB.68"))) {	 //$NON-NLS-1$
		    	password = p_deObfuscate(password);
		    }

		    

		    if (serverUrl.startsWith("ssl://")) {
		    	// set ssl options
		    	ssl_vendor = c.getProperty("ssl_vendor", "j2se");
		    	// TODO enable tracing from parameter
		    	ssl_trace = false; 
		    	ssl_debug_trace = false; 
		    	String[] ssl_trustedList = c.getProperty("ssl_trusted", "").split(","); // comma-separated list
		    	for (int i = 0; i < ssl_trustedList.length && !ssl_trustedList[i].equals(""); i++) {
		    		com.tibco.tibjms.TibjmsSSL.addTrustedCerts(ssl_trustedList[i]);
				}
		    	ssl_hostname = c.getProperty("ssl_hostname", "server");
		    	ssl_identity = c.getProperty("ssl_identity", null);
		    	ssl_key = c.getProperty("ssl_key", null);
		    	ssl_password = c.getProperty("ssl_password", null);
			    if (ssl_password != null && ssl_password.startsWith(Msg.get("EB.68"))) {	 //$NON-NLS-1$
			    	ssl_password = p_deObfuscate(ssl_password);
			    }
		    	disable_verify_host_name = new Boolean(c.getProperty("disable_verify_host_name", "true"));
		    	disable_verify_host = new Boolean(c.getProperty("disable_verify_host", "true"));
		    	ssl_ciphers = c.getProperty("ssl_ciphers", "");	
		    }
		    
	    }
	    catch (RuntimeException r) {
			r.printStackTrace();
		}
	    catch (Exception e) {
			e.printStackTrace();
		}
	    finally {
	    	if (scanner != null)
	    		{ scanner.close(); }
	    }			
	}

	
	// TODO refactor
	public Hashtable<Object, Object> InitSSL() {
        // initialize SSL environment
        Hashtable<Object, Object> environment = new Hashtable<Object, Object>();
        try {

            // set SSL vendor
            if (ssl_vendor != null) {
                environment.put(com.tibco.tibjms.TibjmsSSL.VENDOR, ssl_vendor);
            }

            // set trace for client-side operations, loading of certificates
            // and other
            if (ssl_trace) {
                environment.put(com.tibco.tibjms.TibjmsSSL.TRACE, new Boolean(true));
            }

            // set vendor trace. Has no effect for "j2se", "entrust6" uses
            // this to trace SSL handshake
            if (ssl_debug_trace) {
                environment.put(com.tibco.tibjms.TibjmsSSL.DEBUG_TRACE, new Boolean(true));
            }

            // set trusted certificates if specified
            if (ssl_trusted.size() != 0) {
                environment.put(com.tibco.tibjms.TibjmsSSL.TRUSTED_CERTIFICATES, ssl_trusted);
            }

            // set expected host name in the certificate if specified
            if (ssl_hostname != null) {
                environment.put(com.tibco.tibjms.TibjmsSSL.EXPECTED_HOST_NAME, ssl_hostname);
            }

            //
            if (ssl_ciphers != null) {
                environment.put(com.tibco.tibjms.TibjmsSSL.CIPHER_SUITES, ssl_ciphers);
            }

            if (disable_verify_host_name) {
              environment.put(com.tibco.tibjms.TibjmsSSL.ENABLE_VERIFY_HOST_NAME, new Boolean(false));
            }

            if (disable_verify_host || ssl_trusted.size()==0) {
               environment.put(com.tibco.tibjms.TibjmsSSL.ENABLE_VERIFY_HOST, new Boolean(false));
            }

            // set client identity if specified. ssl_key may be null
            // if identity is PKCS12, JKS or EPF. 'j2se' only supports
            // PKCS12 and JKS. 'entrust6' also supports PEM/PKCS8 combination.
            if (ssl_identity != null) {
                if (ssl_password == null) {
                    System.err.println("Error: must specify -ssl_password if identity is set");
                    System.exit(-1);
                }
                environment.put(com.tibco.tibjms.TibjmsSSL.IDENTITY, ssl_identity);
                environment.put(com.tibco.tibjms.TibjmsSSL.PASSWORD, ssl_password);

                if (ssl_key != null)
                   environment.put(com.tibco.tibjms.TibjmsSSL.PRIVATE_KEY, ssl_key);
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            if (e instanceof JMSException) {
                JMSException je = (JMSException)e;
                if (je.getLinkedException() != null) {
                    System.err.println("##### Linked Exception:");
                    je.getLinkedException().printStackTrace();
                }
            }
            System.exit(-1);
        }
		
		return environment;
	}
	
	
	public String obfuscate(final String password) throws Exception {
	    Properties butlerProps = new Properties();
	    butlerProps.load(this.getClass().getResourceAsStream("JMSButler.properties"));
		String passPhrase   = butlerProps.getProperty("passPhrase");
		GenUtils desEncrypter = new GenUtils(passPhrase);
	    String ep = Msg.get("EB.71") + desEncrypter.encrypt(password); //$NON-NLS-1$
	    System.out.println(ep);    
	    return ep;
	}

	public static String genObfuscate(final String password) throws Exception {
		JMSButler eb = new JMSButler(0);
		return eb.obfuscate(password);
	}
	
	private String p_deObfuscate(final String password) throws Exception {
	    String passPhrase = getCredentialsPassphrase();
		GenUtils desEncrypter = new GenUtils(passPhrase);
	    String ctp = password.substring(2);
	    return desEncrypter.decrypt(ctp);
	}

	/**
	 * @return
	 * @throws IOException
	 */
	private String getCredentialsPassphrase() throws IOException {
		Properties butlerProps = new Properties();
	    butlerProps.load(this.getClass().getResourceAsStream("JMSButler.properties"));
		String passPhrase   = butlerProps.getProperty("passPhrase");
		return passPhrase;
	}	
	
//	ClassLoader.getResourceAsStream ("some/pkg/resource.properties");
//	  Class.getResourceAsStream ("/some/pkg/resource.properties");
//	  ResourceBundle.getBundle ("some.pkg.resource");
//	Class.getResourceAsStream ("resource.properties");

	
	   public static Properties load(String propsName) throws Exception {
	        Properties props = new Properties();
	        URL url = ClassLoader.getSystemResource(propsName);
	        props.load(url.openStream());
	        return props;
	    }

	/**
	 * @param errors the errors to set
	 */
	public void setErrors(ArrayList<String> errors) {
		this.errors = errors;
	}

	/**
	 * @return the errors
	 */
	public ArrayList<String> getErrors() {
		return errors;
	}

	/**
	 * @param commandFile the commandFile to set
	 */
	public void setCommandFile(String commandFile) {
		this.commandFile = commandFile;
	}

	/**
	 * @return the commandFile
	 */
	public String getCommandFile() {
		return commandFile;
	}

	private String p_getPassphrase() {
		String p1 = "Moad-HJjY-QGEX#";
		String p3 = p1.replaceAll("-", "");
		p3 = p3.replaceAll("#", "/") + p2;
		
		//return "MoadHJjYQGEX/AOHytu2QqA==";
		return p3 + "==";
	}

}


