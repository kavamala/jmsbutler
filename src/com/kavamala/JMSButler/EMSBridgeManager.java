package com.kavamala.JMSButler;

import java.util.Properties;

import com.tibco.tibjms.admin.DestinationBridgeInfo;
import com.tibco.tibjms.admin.DestinationInfo;
import com.tibco.tibjms.admin.TibjmsAdmin;
import com.tibco.tibjms.admin.TibjmsAdminException;
import com.tibco.tibjms.admin.TibjmsAdminInvalidNameException;
import com.tibco.tibjms.admin.TibjmsAdminNameExistsException;

public class EMSBridgeManager {
	
	String cmdLine;
	TibjmsAdmin ta;
	DestinationBridgeInfo bi = null;
	
	String bridgeName;
	String command;
	String description;
	Properties bridgeProperties;

	
	public EMSBridgeManager(final String cmdLine, final TibjmsAdmin ta) throws Exception {
		super();
		this.cmdLine = cmdLine;
		this.ta = ta;
		p_init(cmdLine);

	}


	public void createOrUpdateBridge() throws Exception {
		p_createOrUpdateBridge();
	}


	public void deleteBridge() throws Exception {
		p_deleteBridge();
	}


	/**
	 * @param cmdLine
	 * @throws Exception
	 */
	private void p_init(final String cmdLine) throws Exception {
		EMSUtils utils = new EMSUtils();
		bridgeProperties = utils.simpleCommandParser(cmdLine);
		bridgeName = bridgeProperties.getProperty("__objName");
		command = bridgeProperties.getProperty("__command");
//		for (Iterator<?> i = bridgeProperties.keySet().iterator(); i.hasNext();) {
//			String key = (String) i.next();
//			if (key.startsWith("\"") && key.endsWith("\"")) {
//				description = key.substring(1, key.length()-1);
//			}
//		}
		if(!utils.syntaxcheck(command, "bridge", cmdLine)) {
			throw new JMSButlerException("incorrect synthax");
		}

	}


	/**
	 * @throws TibjmsAdminException
	 * @throws TibjmsAdminInvalidNameException
	 * @throws TibjmsAdminNameExistsException
	 */
	private void p_createOrUpdateBridge() throws TibjmsAdminException,
			TibjmsAdminInvalidNameException, Exception {
		
		String[] src = bridgeProperties.getProperty(EMSConstants.Source, ":").split(":");
		String[] trg = bridgeProperties.getProperty(EMSConstants.Target, ":").split(":");

//		try {
			//debug
			System.out.println("EB: processing bridge "+ bridgeName + " target=" +bridgeProperties.getProperty(EMSConstants.Target, ":"));

			int srcType = (src[0].equalsIgnoreCase(EMSConstants.Queue) ? DestinationInfo.QUEUE_TYPE : DestinationInfo.TOPIC_TYPE);
			int trgType = (trg[0].equalsIgnoreCase(EMSConstants.Queue) ? DestinationInfo.QUEUE_TYPE : DestinationInfo.TOPIC_TYPE);
			
			String selector;
			
			try {
				selector = bridgeProperties.getProperty(EMSConstants.Selector, null);
				if (selector != null) {
					selector = selector.replaceAll("~", " ").replaceAll(";", ",");
					selector = selector.substring(1, selector.length()-1);
				}
			} catch (Exception e) {
				throw new JMSButlerException("incorrect synthax", e);
			}

			bi=ta.getDestinationBridge(srcType, src[1], trgType, trg[1]);

			if (bi == null) {   
				//System.out.println("-- bridge does not exists");
				bi = new DestinationBridgeInfo(srcType, src[1], trgType, trg[1], selector);
				ta.createDestinationBridge(bi);	    	
			} else {
				//System.out.println("found bridge " + bridgeName);
				// update selector (need to recreate the bridge
				String oldSelector = bi.getSelector();
//	    	System.out.println(selector);
//	    	System.out.println(oldSelector);
				if (oldSelector!= null && !selector.equals(oldSelector)) {
					ta.destroyDestinationBridge(srcType, src[1], trgType, trg[1]);
			 		ta.createDestinationBridge(new DestinationBridgeInfo(srcType, src[1], trgType, trg[1], selector));
			 	}
			} 
			
			//debug
			System.out.println("EB: bridge "+ bridgeName +  " target=" +bridgeProperties.getProperty(EMSConstants.Target, ":") + " created/updated");
//		} catch (Exception e) {
//			System.out.println(e.getMessage());
//			throw e;
//		}
	}


	/**
	 * @throws TibjmsAdminException
	 * @throws TibjmsAdminInvalidNameException
	 */
	private void p_deleteBridge() throws TibjmsAdminException,
			TibjmsAdminInvalidNameException {
		//debug
		System.out.println("EB: processing bridge "+ bridgeName );
		
		String[] src = bridgeProperties.getProperty(EMSConstants.Source, ":").split(":");
		String[] trg = bridgeProperties.getProperty(EMSConstants.Target, ":").split(":");
		int srcType = (src[0].equalsIgnoreCase(EMSConstants.Queue) ? DestinationInfo.QUEUE_TYPE : DestinationInfo.TOPIC_TYPE);
		int trgType = (trg[0].equalsIgnoreCase(EMSConstants.Queue) ? DestinationInfo.QUEUE_TYPE : DestinationInfo.TOPIC_TYPE);
	   	
		ta.destroyDestinationBridge(srcType, src[1], trgType, trg[1]);
		
	   	//debug
		System.out.println("EB: bridge "+ bridgeName +" deleted");
	}
	
}
