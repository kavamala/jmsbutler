package com.kavamala.JMSButler;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Properties;

import com.tibco.tibjms.admin.ACLEntry;
import com.tibco.tibjms.admin.DestinationInfo;
import com.tibco.tibjms.admin.Permissions;
import com.tibco.tibjms.admin.PrincipalInfo;
import com.tibco.tibjms.admin.TibjmsAdmin;
import com.tibco.tibjms.admin.TibjmsAdminException;
import com.tibco.tibjms.admin.TibjmsAdminInvalidNameException;
import com.tibco.tibjms.admin.TibjmsAdminNameExistsException;

/**
 * @author Daniele Galluccio
 * 
 * 
 * v.1.2 
 * Fixed behavior with revoke permission
 * 
 */
public class EMSPermissionManager {
	
	String cmdLine;
	TibjmsAdmin ta;
	ACLEntry[] prmss = null;
	
	String objectName;
	String objectType;
	String command;
	String description;
	Properties permissionProperties;
	ArrayList<ACLEntry> ACLs = new ArrayList<ACLEntry>();

	
	public EMSPermissionManager(final String cmdLine, final TibjmsAdmin ta) throws Exception {
		super();
		this.cmdLine = cmdLine;
		this.ta = ta;
		EMSUtils utils = new EMSUtils();
		permissionProperties = utils.propertiesInit(cmdLine);
		objectName = permissionProperties.getProperty("__objName");
		command = permissionProperties.getProperty("__command");
		description = permissionProperties.getProperty("__description");
		objectType = permissionProperties.getProperty("__objType");
	}


	public void setPermissions() throws Exception {
		p_setPermissions();
	}



	/**
	 * @throws TibjmsAdminException
	 * @throws TibjmsAdminInvalidNameException
	 * @throws TibjmsAdminNameExistsException
	 * @throws JMSButlerException 
	 */
	private void p_setPermissions() throws TibjmsAdminException,
			TibjmsAdminInvalidNameException, TibjmsAdminNameExistsException, JMSButlerException {
		//debug
		System.out.println(Msg.get("JB.prefix")+ "processing ACL "+ objectName );

		Object obj = null;
		PrincipalInfo principal = null;
		boolean grantOrRevoke = command.equalsIgnoreCase("grant");
		
		if (permissionProperties.getProperty("group") != null) {
			principal = ta.getGroup(permissionProperties.getProperty("group"));
		} else if (permissionProperties.getProperty("user") != null) {
			principal = ta.getUser(permissionProperties.getProperty("user"));
		} 
		if (principal == null) {
			throw new JMSButlerException(Msg.get("JB.error.nullPrincipal"));
		}

		if (objectType.equalsIgnoreCase(EMSConstants.Queue)) {
			obj = ta.getQueue(objectName);
			p_grantACL(obj, principal, grantOrRevoke);
		} else if (objectType.equalsIgnoreCase(EMSConstants.Topic)) {
			obj = ta.getTopic(objectName);
			p_grantACL(obj, principal, grantOrRevoke);
		} else if (objectType.equalsIgnoreCase(EMSConstants.Admin)) {
			// uses AdminACLEntry a;
			// FIXME Admin privileges
			throw new JMSButlerException("For security reasons, grant of admin privileges via JMSButler is not allowed");
		} 
		


	}


	/**
	 * @param obj 			Queue, Topic, 
	 * @param principal		User, Group
	 * @throws TibjmsAdminException
	 * @throws JMSButlerException 
	 */
	private void p_grantACL(Object obj, PrincipalInfo principal, boolean grantOrRevoke) throws TibjmsAdminException, JMSButlerException {
		
		
		ACLEntry[] acls = ta.getACLEntries();
		Permissions px = null;
		for (int i = 0; i < acls.length; i++) {
			String princ = acls[i].getPrincipal().getName();
			if (princ.equals(principal.getName())) {
				//String perm = acls[i].getPermissions().toString();
				//System.out.println("* "+ princ + " : " + perm );
				px = acls[i].getPermissions();
//				px.hasPermission(arg0);
			}			
		}
		
		ACLEntry acl = null;
		for (Iterator<?> i = permissionProperties.keySet().iterator(); i.hasNext();) {
			String key = (String) i.next();
			String value = permissionProperties.getProperty(key);
			if (!key.equals(objectName) &&
				!key.equals(objectType) &&
				value.equals("true")) {
				if (px.hasPermission(p_iLookUp(key, objectType))) {
					//System.out.println("debug: property present" );
				}
				//acl = new ACLEntry((DestinationInfo) obj, principal, p_lookUp(key, objectType, grantOrRevoke));
				acl = new ACLEntry((DestinationInfo) obj, principal, p_lookUp(key, objectType, true)); // apparently API only works with true (both grant and revoke)
				ACLs.add(acl);
			}
//			acl = new ACLEntry((DestinationInfo) obj, principal, p_lookUp(key, objectType, grantOrRevoke));
//			ACLs.add(acl);
		}
		
		for (Iterator<ACLEntry> a = ACLs.iterator(); a.hasNext();) {
			ACLEntry entry = a.next();
			try {
				if (command.equalsIgnoreCase("grant")){
					ta.grant(entry);	
				} else if (command.equalsIgnoreCase("revoke")) {
					ta.revoke(entry);
				} else {
					//do nothing (ignore malformed?)
				}
			} catch (TibjmsAdminException e) {
				System.err.println(e.getLocalizedMessage());
				System.err.println(e.getCause().getMessage());
//				System.out.println(((DestinationInfo) obj).getName());
//				System.out.println(principal.getName());
				entry.toString();
				throw e;
			}
		}
//		ta.grant((ACLEntry[]) ACLs.toArray());
	}


	
	
	private Permissions p_lookUp(String p, String objectType, boolean grantOrRevoke) throws JMSButlerException {

		Permissions pp = new Permissions();
		//long prm = -1;
		
		if (objectType.equalsIgnoreCase("queue")) {
			if (p.equalsIgnoreCase("browse")) { pp.setPermission(Permissions.BROWSE, grantOrRevoke); }
			else if (p.equalsIgnoreCase("create")) { pp.setPermission(Permissions.CREATE, grantOrRevoke); }
			else if (p.equalsIgnoreCase("delete")) { pp.setPermission(Permissions.DELETE, grantOrRevoke); }
			else if (p.equalsIgnoreCase("modify")) { pp.setPermission(Permissions.MODIFY, grantOrRevoke); }
			else if (p.equalsIgnoreCase("purge")) { pp.setPermission(Permissions.PURGE, grantOrRevoke); }
			else if (p.equalsIgnoreCase("receive")) { pp.setPermission(Permissions.RECEIVE, grantOrRevoke); }
			else if (p.equalsIgnoreCase("send")) { pp.setPermission(Permissions.SEND, grantOrRevoke); }
			else if (p.equalsIgnoreCase("view")) { pp.setPermission(Permissions.VIEW, grantOrRevoke); }
			else if (p.equalsIgnoreCase("all")) { 
				pp.setPermission(Permissions.RECEIVE, grantOrRevoke);
				pp.setPermission(Permissions.SEND, grantOrRevoke);
				pp.setPermission(Permissions.BROWSE, grantOrRevoke);
			}
			else {
				throw new JMSButlerException("permission type: " + p + " not supported for object " + objectType);
			}			
		} else if (objectType.equalsIgnoreCase("topic")) {
			if (p.equalsIgnoreCase("create")) { pp.setPermission(Permissions.CREATE, grantOrRevoke); }
			else if (p.equalsIgnoreCase("delete")) { pp.setPermission(Permissions.DELETE, grantOrRevoke); }
			else if (p.equalsIgnoreCase("durable")) { pp.setPermission(Permissions.DURABLE, grantOrRevoke); }
			else if (p.equalsIgnoreCase("modify")) { pp.setPermission(Permissions.MODIFY, grantOrRevoke); }
			else if (p.equalsIgnoreCase("publish")) { pp.setPermission(Permissions.PUBLISH, grantOrRevoke); }
			else if (p.equalsIgnoreCase("purge")) { pp.setPermission(Permissions.PURGE, grantOrRevoke); }
			else if (p.equalsIgnoreCase("subscribe")) { pp.setPermission(Permissions.SUBSCRIBE, grantOrRevoke); }
			else if (p.equalsIgnoreCase("use_durable")) { pp.setPermission(Permissions.USE_DURABLE, grantOrRevoke); }
			else if (p.equalsIgnoreCase("view")) { pp.setPermission(Permissions.VIEW, grantOrRevoke); }
			else if (p.equalsIgnoreCase("all")) { 
				pp.setPermission(Permissions.SUBSCRIBE, grantOrRevoke);
				pp.setPermission(Permissions.PUBLISH, grantOrRevoke);
				pp.setPermission(Permissions.USE_DURABLE, grantOrRevoke);
				pp.setPermission(Permissions.DURABLE, grantOrRevoke);
			}
			else {
				throw new JMSButlerException("permission type: " + p + " not supported for object " + objectType);
			}
			
		} else if (objectType.equalsIgnoreCase("admin")) {
			throw new JMSButlerException("admin permissions not supported");
		} else {
			throw new JMSButlerException("unknown object type");
		}
			
		return pp;
	}


	private long p_iLookUp(String p, String objectType) throws JMSButlerException {
	
		
		long prm = -1;
		if (objectType.equalsIgnoreCase("queue")) {
			if (p.equalsIgnoreCase("browse")) { prm =Permissions.BROWSE; }
			else if (p.equalsIgnoreCase("create")) { prm =Permissions.CREATE; }
			else if (p.equalsIgnoreCase("delete")) { prm =Permissions.DELETE; }
			else if (p.equalsIgnoreCase("modify")) { prm =Permissions.MODIFY; }
			else if (p.equalsIgnoreCase("purge")) { prm =Permissions.PURGE; }
			else if (p.equalsIgnoreCase("receive")) { prm =Permissions.RECEIVE; }
			else if (p.equalsIgnoreCase("send")) { prm =Permissions.SEND; }
			else if (p.equalsIgnoreCase("view")) { prm =Permissions.VIEW; }
			else if (p.equalsIgnoreCase("all")) { 
				prm =Permissions.RECEIVE
				& Permissions.SEND
				& Permissions.BROWSE;
			}
			else {
				throw new JMSButlerException("permission type: " + p + " not supported for object " + objectType);
			}			
		} else if (objectType.equalsIgnoreCase("topic")) {
			if (p.equalsIgnoreCase("create")) { prm =Permissions.CREATE; }
			else if (p.equalsIgnoreCase("delete")) { prm =Permissions.DELETE; }
			else if (p.equalsIgnoreCase("durable")) { prm =Permissions.DURABLE; }
			else if (p.equalsIgnoreCase("modify")) { prm =Permissions.MODIFY; }
			else if (p.equalsIgnoreCase("publish")) { prm =Permissions.PUBLISH; }
			else if (p.equalsIgnoreCase("purge")) { prm =Permissions.PURGE; }
			else if (p.equalsIgnoreCase("subscribe")) { prm =Permissions.SUBSCRIBE; }
			else if (p.equalsIgnoreCase("use_durable")) { prm =Permissions.USE_DURABLE; }
			else if (p.equalsIgnoreCase("view")) { prm =Permissions.VIEW; }
			else if (p.equalsIgnoreCase("all")) { 
				prm = (Permissions.SUBSCRIBE & Permissions.PUBLISH
				& Permissions.USE_DURABLE
				& Permissions.DURABLE);
			}
			else {
				throw new JMSButlerException("permission type: " + p + " not supported for object " + objectType);
			}
			
		} else if (objectType.equalsIgnoreCase("admin")) {
			throw new JMSButlerException("admin permissions not supported");
		} else {
			throw new JMSButlerException("unknown object type");
		}
		
		return prm;
	}	
}
