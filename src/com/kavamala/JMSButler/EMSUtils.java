/**
 * 
 */
package com.kavamala.JMSButler;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Properties;
import java.util.StringTokenizer;


/**
 * @author Administrator
 *
 */
public final class EMSUtils {

	// Public Methods
	// ***************************************************************
	public static long parseSize (String size) throws Exception {
		return p_parseSize(size);
	} // End parseSize

	public static long parseTime (String time) throws Exception {
		return p_parseTime(time);
	} // End parseSize

	public static Properties parseArgs (String[] args) throws Exception {
		return p_parseArgs(args);
	} // End parseArgs
	
	private static Properties p_parseArgs(String[] args) {
		Properties p = new Properties();
        int i=0;

        while(args != null && i < args.length)
        {
            if (args[i].compareTo(Msg.get("0.0"))==0) { //$NON-NLS-1$
                if ((i+1) >= args.length) usage();
                p.setProperty(Msg.get("0.1"), args[i+1]); //$NON-NLS-1$
                i += 2;
            } else if (args[i].compareTo(Msg.get("0.2"))==0) { //$NON-NLS-1$
                if ((i+1) >= args.length) usage();
                p.setProperty(Msg.get("0.3"), args[i+1]); //$NON-NLS-1$
                i += 2;
            } else if (args[i].compareTo(Msg.get("0.4"))==0) { //$NON-NLS-1$
                if ((i+1) >= args.length) usage();
                p.setProperty(Msg.get("0.5"), args[i+1]); //$NON-NLS-1$
                i += 2;
            } else if (args[i].compareTo(Msg.get("0.6"))==0) { //$NON-NLS-1$
                if ((i+1) >= args.length) usage();
                p.setProperty(Msg.get("0.7"), args[i+1]); //$NON-NLS-1$
                i += 2;
            } else if (args[i].compareTo(Msg.get("0.8"))==0) { //$NON-NLS-1$
                if ((i+1) >= args.length) usage();
                p.setProperty(Msg.get("0.9"), args[i+1]); //$NON-NLS-1$
                i += 2;
            } else if (args[i].compareTo(Msg.get("0.10"))==0) { //$NON-NLS-1$
                if ((i+1) >= args.length) usage();
                p.setProperty(Msg.get("0.11"), args[i+1]); //$NON-NLS-1$
                i += 2;
            } else  if (args[i].compareTo(Msg.get("0.12"))==0) { //$NON-NLS-1$
                usage();
                break;
            }  else {
                System.err.println(Msg.get("0.13")+args[i]); //$NON-NLS-1$
                usage();
                break;
            }
        }
		
		return p;
	}

	private static void usage() {
		//FIXME write usage help!
		System.out.println(Msg.get("0.14")); //$NON-NLS-1$
		
	}

	/**
	 * Method to initialise program arguments
	 * 
	 * @param cmdLine
	 * @return
	 * @throws Exception
	 */
	public Properties propertiesInit(final String cmdLine) throws Exception {
		return p_init(cmdLine);
	} // end propertiesInit

	public Properties simpleCommandParser(String cmdLine) throws Exception  {
		return p_simpleParseCmdLine(cmdLine);
	}

	
	

	public String normalizeCmdLine(String line) {
		return p_normaliseCmdLine(line);
	}

	/**
	 * @param time
	 * @return
	 */
	private static long p_parseTime(String time) {
		long secs = -1;
	
		if (time.endsWith(Msg.get("0.15"))) { //$NON-NLS-1$
			String t = time.substring(0, (time.length()-4));
			secs = (long) Long.parseLong(t) / 1000;			
		} else if (time.endsWith(Msg.get("0.16"))) { //$NON-NLS-1$
			secs = Long.parseLong(time.substring(0, time.length()-3));			
		} else if (time.endsWith(Msg.get("0.17"))) { //$NON-NLS-1$
			secs = Long.parseLong(time.substring(0, time.length()-3)) * 60;			
		} else if (time.endsWith(Msg.get("0.18"))) { //$NON-NLS-1$
			secs = Long.parseLong(time.substring(0, time.length()-4)) * 60 * 60;			
		} else if (time.endsWith(Msg.get("0.19"))) { //$NON-NLS-1$
			secs = Long.parseLong(time.substring(0, time.length()-3)) * 86400;			
		} else {
			secs = Long.parseLong(time);
		}
		return secs;
	}

	/**
	 * @param size
	 * @return
	 */
	private static long p_parseSize(String size) {
		long bytes = -1;
	
		if (size.endsWith(Msg.get("0.20"))) { //$NON-NLS-1$
			bytes = Long.parseLong(size.substring(0, size.length()-2)) * 1024;			
		} else if (size.endsWith(Msg.get("0.21"))) { //$NON-NLS-1$
			bytes = Long.parseLong(size.substring(0, size.length()-2)) * 1024 * 1024;			
		} else if (size.endsWith(Msg.get("0.22"))) { //$NON-NLS-1$
			bytes = Long.parseLong(size.substring(0, size.length()-2)) * 1024 * 1024 * 1024;			
		} else {
			bytes = Long.parseLong(size);
		}
		return bytes;
	}

	// Private Methods
		// ***************************************************************
		private Properties p_simpleParseCmdLine(String cmdLine) throws Exception {

			StringTokenizer st = new StringTokenizer(p_normaliseCmdLine(cmdLine));	
			Properties p = new Properties();
	
			int i =0;

			while (st.hasMoreElements()) {
				switch (i) {
					case 0:
						p.setProperty(Msg.get("0.23"), st.nextToken()); //$NON-NLS-1$
						break;
					case 1:
						// usually reserved word (queue, topic, user, bridge ...)
						p.setProperty(Msg.get("0.24"), st.nextToken()); //$NON-NLS-1$
						break;
					default:	
						String plist = st.nextToken();
						if (i==2) {
							p.setProperty(Msg.get("0.25"), plist); //$NON-NLS-1$
						} 
		
						StringTokenizer pt = new StringTokenizer(plist,Msg.get("0.26")); //$NON-NLS-1$
						while (pt.hasMoreElements()) {
							String a = pt.nextToken();
							if (a.contains(Msg.get("0.27"))) { //$NON-NLS-1$
								String[] b = a.split(Msg.get("0.28")); //$NON-NLS-1$
								p.setProperty(b[0].toLowerCase(), b[1].replaceAll(Msg.get("0.29"), Msg.get("0.30"))); //$NON-NLS-1$ //$NON-NLS-2$
							} else {
								p.setProperty(a.toLowerCase(), Msg.get("0.31")); //$NON-NLS-1$
							}						
						}					
		
						break;
				} // end switch
				i++;
			}
			return p;
		}

	/**
	 * @
	 * Fixes irregular occurences of separator characters es '=' and ','
	 * within the ems command.
	 * This makes subsequent parsing simpler
	 * 
	 * @param cmdLine	EMS syntax command
	 * @return String	Modified command line
	 */		
	private String p_normaliseCmdLine(String line) {
		boolean inQuotes = false;
		char quote = '"';
		String newCmdLine = Msg.get("0.32"); //$NON-NLS-1$
		for (int i = 0; i < line.length(); i++) {
			if (line.charAt(i) == quote) {
				inQuotes = !inQuotes;
				newCmdLine +=line.charAt(i);
			} else if (line.charAt(i) == ',' && inQuotes) {
				newCmdLine += Msg.get("0.33"); //$NON-NLS-1$
			} else if (line.charAt(i) == '=' && inQuotes) {
				newCmdLine += Msg.get("0.34"); //$NON-NLS-1$
			} else if (line.charAt(i) == ' ' && inQuotes) {
				newCmdLine += Msg.get("0.35"); //$NON-NLS-1$
			} else {
				newCmdLine +=line.charAt(i);
			}

		}
		return newCmdLine;
	} // end p_normaliseCmdLine
	
	/**
	 * @param cmdLine
	 * @throws Exception
	 */
	private Properties p_init(final String cmdLine) throws Exception {
		Properties objProperties = simpleCommandParser(cmdLine);
		//topicName = objProperties.getProperty("__objName");
		//command = objProperties.getProperty("__command");
		ArrayList<String> p = new ArrayList<String>();
		for (Iterator<?> i = objProperties.keySet().iterator(); i.hasNext();) {
			String key = (String) i.next();
			p.add(key);
		}
		for (String key : p) {
			if (key.startsWith(Msg.get("0.36")) && key.endsWith(Msg.get("0.37"))) { //$NON-NLS-1$ //$NON-NLS-2$
//				description = key.substring(1, key.length()-1);
				objProperties.setProperty(Msg.get("0.38"),key.substring(1, key.length()-1)); //$NON-NLS-1$
			}
			String v = objProperties.getProperty(key);
			if (v.startsWith(Msg.get("0.39")) && v.endsWith(Msg.get("0.40"))) { //$NON-NLS-1$ //$NON-NLS-2$
				objProperties.setProperty(key, v.substring(1, v.length()-1));
			}			
		}
//		for (Iterator<?> i = objProperties.keySet().iterator(); i.hasNext();) {
//			String key = (String) i.next();
//			if (key.startsWith("\"") && key.endsWith("\"")) {
////				description = key.substring(1, key.length()-1);
//				objProperties.setProperty("__description",key.substring(1, key.length()-1));
//			}
//			String v = objProperties.getProperty(key);
//			if (v.startsWith("\"") && v.endsWith("\"")) {
//				objProperties.setProperty(key, v.substring(1, v.length()-1));
//			}
//		}
		return objProperties;
	} // end p_init		
	
	
	public boolean syntaxcheck (final String command, final String obj, final String line) {
		return p_syntaxcheck(command, obj, line);
	}
	
	private boolean p_syntaxcheck (final String command, final String obj, final String line) {
		boolean synthaxOK = true;
		//TODO refactor me
		if (command.equalsIgnoreCase("create")) {
			if (obj.equalsIgnoreCase("bridge")) {
				// use regex patterns?
				if (line.matches(".*[Cc]reate bridge.*[Cc]reate bridge.*")) {
					synthaxOK = false;
				}
				
			}
		}
		
		return synthaxOK;
	}

}
