/**
 * 
 */
package com.kavamala.JMSButler;


/**
 * @author Administrator
 *
 */
public class JMSButlerException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7181063004680228618L;

	StackTraceElement[] st = null;
	public JMSButlerException(Exception e) {
		super(e);
	}

	public JMSButlerException(String string, Exception e) {
		super(string,e);
	}

	public JMSButlerException(String string) {
		super(string);
	}


}
