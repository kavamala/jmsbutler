package com.kavamala.JMSButler;

import java.util.Iterator;
import java.util.Properties;

import com.tibco.tibjms.admin.TibjmsAdmin;
import com.tibco.tibjms.admin.TibjmsAdminException;
import com.tibco.tibjms.admin.TibjmsAdminInvalidNameException;
import com.tibco.tibjms.admin.TibjmsAdminNameExistsException;
import com.tibco.tibjms.admin.UserInfo;

public class EMSUserManager {
	
	String cmdLine;
	TibjmsAdmin ta;
	UserInfo ui = null;
	
	String userName;
	String command;
	String description;
	Properties userProperties;

	
	public EMSUserManager(final String cmdLine, final TibjmsAdmin ta) throws Exception {
		super();
		this.cmdLine = cmdLine;
		this.ta = ta;
		p_init(cmdLine);

	}


	public void createOrUpdateUser() throws Exception {
		p_createOrUpdateUser();
	}


	public void deleteUser() throws Exception {
		p_deleteUser();
	}


	/**
	 * @param cmdLine
	 * @throws Exception
	 */
	private void p_init(final String cmdLine) throws Exception {
		EMSUtils utils = new EMSUtils();
		userProperties = utils.simpleCommandParser(cmdLine);
		userName = userProperties.getProperty("__objName");
		command = userProperties.getProperty("__command");
		for (Iterator<?> i = userProperties.keySet().iterator(); i.hasNext();) {
			String key = (String) i.next();
			if (key.startsWith("\"") && key.endsWith("\"")) {
				description = key.substring(1, key.length()-1);
			}
		}
	}


	/**
	 * @throws TibjmsAdminException
	 * @throws TibjmsAdminInvalidNameException
	 * @throws TibjmsAdminNameExistsException
	 */
	private void p_createOrUpdateUser() throws TibjmsAdminException,
			TibjmsAdminInvalidNameException, TibjmsAdminNameExistsException {
		//debug
		System.out.println("EB: processing User "+ userName );
		
	   	ui=ta.getUser(userName);
	    if (ui == null) {   
	    	//System.out.println("-- queue does not exists");
	    	ui = new UserInfo(userName);
	    	ui.setPassword(userProperties.getProperty(EMSConstants.Password, null));
	    	ui.setDescription(description);
	    	ta.createUser(ui);
	    	
	    } else {
	    	//System.out.println("found topic " + userName);
	    	ui.setPassword(userProperties.getProperty(EMSConstants.Password, null));
	    	ui.setDescription(description);
	    	ta.updateUser(ui);
	    } 
		
		//debug
		System.out.println("EB: user "+ userName +" created/updated");
	}


	/**
	 * @throws TibjmsAdminException
	 * @throws TibjmsAdminInvalidNameException
	 */
	private void p_deleteUser() throws TibjmsAdminException,
			TibjmsAdminInvalidNameException {
		//debug
		System.out.println("EB: processing user "+ userName );
		
	   	ta.destroyUser(userName);
	   	
	   	//debug
		System.out.println("EB: user "+ userName +" deleted");
	}


	public void deleteAllUsers() throws Exception {
		p_deleteAllUsers();
	}


	private void p_deleteAllUsers() throws TibjmsAdminException {
		UserInfo uis[] = ta.getUsers();
		for (int i = 0; i < uis.length; i++) {
			ta.destroyUser(uis[i].getName());
		}
	}

/*
	private void parseCmdLine() {
		final StringTokenizer st = new StringTokenizer(cmdLine);	
		int i =0;
		while (st.hasMoreElements()) {
			switch (i) {
			case 0:
				command = st.nextToken();
				break;
			case 1:
				// topic res word 
				st.nextToken();
				break;
			case 2:
				userName = st.nextToken();
				break;
			default:
				final String plist = st.nextToken();
				if (plist.contains("\"")) {
					// use advanced parser
					System.out.println("-- inlist not supported, skipped parsing");
				} else {
					final StringTokenizer pt = new StringTokenizer(plist,",");
					while (pt.hasMoreElements()) {
						final String a = pt.nextToken();
						if (a.contains("=")) {
							final String[] b = a.split("=");
							userProperties.setProperty(b[0].toLowerCase(), b[1]);
						} else {
							userProperties.setProperty(a.toLowerCase(), "true");
						}						
					}					
				}
				break;
			}
			i++;
		}
	}
*/	
}
