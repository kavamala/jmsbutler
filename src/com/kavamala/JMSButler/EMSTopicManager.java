package com.kavamala.JMSButler;

import java.util.Properties;

import com.tibco.tibjms.admin.DestinationInfo;
import com.tibco.tibjms.admin.TibjmsAdmin;
import com.tibco.tibjms.admin.TibjmsAdminException;
import com.tibco.tibjms.admin.TibjmsAdminInvalidNameException;
import com.tibco.tibjms.admin.TopicInfo;

public final class EMSTopicManager {
	
	String cmdLine;
	TibjmsAdmin ta;
	TopicInfo ti = null;
	
	String topicName;
	String command;
	Properties topicProperties; // = new Properties();
	
	public EMSTopicManager(String cmdLine, TibjmsAdmin ta) throws Exception {
		super();
		this.cmdLine = cmdLine;
		this.ta = ta;
		//parseCmdLine();
		EMSUtils utils = new EMSUtils();
		topicProperties = utils.propertiesInit(cmdLine);
		topicName = topicProperties.getProperty(Msg.get("2.0")); //$NON-NLS-1$
		command = topicProperties.getProperty(Msg.get("2.1")); //$NON-NLS-1$
	}


	@SuppressWarnings("deprecation")
	public void createOrUpdateTopic() throws Exception {
		//debug
		System.out.println(Msg.get("2.2")+ topicName ); //$NON-NLS-1$
		
	   	ti=ta.getTopic(topicName);
        if (ti == null) {   
        	//System.out.println("-- topic does not exists");
        	ti = new TopicInfo(topicName);
        	ta.createTopic(ti);
        	//System.out.println("topic: "+ topicName +" created");
        } else {
        	//System.out.println("found topic " + topicName);
        }   
  
        //
        //assigning topic properties
        //
                
        ti.setChannel(topicProperties.getProperty(EMSConstants.Channel));
        ti.setDescription(topicProperties.getProperty(Msg.get("2.3"))); 

        String[] Imports = topicProperties.getProperty(EMSConstants.Import, Msg.get("2.4")).split(Msg.get("2.5")); //$NON-NLS-1$ //$NON-NLS-2$
        for (int i = 0; !Imports[i].equals(Msg.get("2.6")) && i < Imports.length; i++) { //$NON-NLS-1$
        	ti.addImportTransport(Imports[i]);
        	//ti.removeImportTransport(arg0)
		}
          
        String[] Exports = topicProperties.getProperty(EMSConstants.Export, Msg.get("2.7")).split(Msg.get("2.8")); //$NON-NLS-1$ //$NON-NLS-2$
        for (int i = 0; !Exports[i].equals(Msg.get("2.9")) && i < Exports.length; i++) { //$NON-NLS-1$
        	ti.addExportTransport(Exports[i]);
        	//ti.removeExportTransport(arg0)
		}

		String Expiration = topicProperties.getProperty(EMSConstants.Expiration, Msg.get("2.10")); //$NON-NLS-1$
		ti.setExpiryOverride(EMSUtils.parseTime(Expiration));

		if (!topicProperties.getProperty(EMSConstants.Store,Msg.get("2.11")).equals(Msg.get("2.12"))) { //$NON-NLS-1$ //$NON-NLS-2$
			ti.setStore(topicProperties.getProperty(EMSConstants.Store));	
		}
        
		String trace = topicProperties.getProperty(EMSConstants.Trace, null);
		if (trace != null) {
			if (trace.equalsIgnoreCase(Msg.get("2.13"))) { //$NON-NLS-1$
				ti.setMsgTrace(DestinationInfo.MSG_TRACE_BASIC);
			} else if (trace.equalsIgnoreCase(Msg.get("2.14"))) { //$NON-NLS-1$
				ti.setMsgTrace(DestinationInfo.MSG_TRACE_DETAIL);
			} else {
				throw new Exception(Msg.get("2.15")); //$NON-NLS-1$
			}
		} else {
			ti.setMsgTrace(DestinationInfo.MSG_TRACE_NONE);
		}        
		
		ti.setFailsafe(Boolean.parseBoolean(topicProperties.getProperty(EMSConstants.Failsafe, Msg.get("2.16")))); //$NON-NLS-1$
		String FlowControl = topicProperties.getProperty(EMSConstants.FlowControlMaxBytes, Msg.get("2.17")); //$NON-NLS-1$
		ti.setFlowControlMaxBytes(EMSUtils.parseSize(FlowControl));
		ti.setGlobal(Boolean.parseBoolean(topicProperties.getProperty(EMSConstants.Global, Msg.get("2.18")))); //$NON-NLS-1$
		String MaxBytes = topicProperties.getProperty(EMSConstants.MaxBytes, Msg.get("2.19")); //$NON-NLS-1$
		ti.setMaxBytes(EMSUtils.parseSize(MaxBytes));
		ti.setMaxMsgs(Long.parseLong(topicProperties.getProperty(EMSConstants.MaxMsgs, Msg.get("2.20"))));	// chk default value //$NON-NLS-1$

		String OverflowPolicy = topicProperties.getProperty(EMSConstants.OverflowPolicy,Msg.get("2.21")); //$NON-NLS-1$
		if (OverflowPolicy.compareToIgnoreCase(Msg.get("2.22")) == 0) { //$NON-NLS-1$
			ti.setOverflowPolicy(DestinationInfo.OVERFLOW_DISCARD_OLD);
		} else if (OverflowPolicy.compareToIgnoreCase(Msg.get("2.23")) == 0) { //$NON-NLS-1$
			ti.setOverflowPolicy(DestinationInfo.OVERFLOW_REJECT_INCOMING);
		} else {
			ti.setOverflowPolicy(DestinationInfo.OVERFLOW_DEFAULT);
		}
		ti.setPrefetch(Integer.parseInt(topicProperties.getProperty(EMSConstants.Prefetch, Msg.get("2.24")+DestinationInfo.PREFETCH_DEFAULT)));	 //$NON-NLS-1$
		ti.setSecure(Boolean.parseBoolean(topicProperties.getProperty(EMSConstants.Secure, Msg.get("2.25")))); //$NON-NLS-1$
		ti.setSenderName(Boolean.parseBoolean(topicProperties.getProperty(EMSConstants.SenderName, Msg.get("2.26")))); //$NON-NLS-1$
		ti.setSenderNameEnforced(Boolean.parseBoolean(topicProperties.getProperty(EMSConstants.SenderNameEnforced, Msg.get("2.27")))); //$NON-NLS-1$
        
		ta.updateTopic(ti);

		System.out.println(Msg.get("2.28")+ topicName +Msg.get("2.29")); //$NON-NLS-1$ //$NON-NLS-2$
	}
	
	public void deleteTopic() throws Exception {
		//debug
		System.out.println(Msg.get("2.30")+ topicName ); //$NON-NLS-1$
		
	   	ta.destroyTopic(topicName);
	   	
	   	//debug
		System.out.println(Msg.get("2.31")+ topicName +Msg.get("2.32")); //$NON-NLS-1$ //$NON-NLS-2$
	}


	public void deleteAllTopics() throws Exception  {
		try {
			p_deleteAllTopics();
		} catch (TibjmsAdminInvalidNameException e) {
			throw e;
		} catch (TibjmsAdminException e) {
			// ignore TibjmsAdminException for this command
		}
	}


	/**
	 * @throws TibjmsAdminInvalidNameException
	 * @throws TibjmsAdminException
	 */
	private void p_deleteAllTopics() throws TibjmsAdminInvalidNameException, TibjmsAdminException {
		String qf[] = cmdLine.split("topics");
		if(qf.length == 2 && !qf[1].trim().equals("")) {
			ta.destroyTopics(qf[1].trim());
		} else {
			ta.destroyTopics(">");
		}
	}
	
}
