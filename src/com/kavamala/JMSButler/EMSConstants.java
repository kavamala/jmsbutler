package com.kavamala.JMSButler;

public interface EMSConstants {
	
	final static String Exclusive = "exclusive";
	final static String Global = "global";
	final static String Failsafe = "failsafe";
	final static String MaxMsgs = "maxmsgs";
	final static String MaxRedelivery = "maxredelivery";
	final static String OverflowPolicy = "overflowpolicy";
	final static String Prefetch = "prefetch";
	final static String Secure = "secure";
	final static String SenderName = "sendername";
	final static String SenderNameEnforced = "sendernameenforced";
	static final String FlowControlMaxBytes = "flowcontrol";
	static final String MaxBytes = "maxbytes";
	static final String Password = "password";
	static final String Expiration = "expiration";
	static final String Import = "import";
	static final String Trace = "trace";
	static final String Store = "store";
	static final String Source = "source";
	static final String Target = "target";
	static final String Queue = "queue";
	static final String Topic = "topic";
	static final String Selector = "selector";
	static final String URL = "url";
	static final String ZoneName = "zone_name";
	static final String ZoneType = "zone_type";
	static final String Channel = "channel";
	static final String Export = "export";
	static final String Admin = "admin";
	static final String dashLine = "---------------------------------------------------------------------------------------";

}
