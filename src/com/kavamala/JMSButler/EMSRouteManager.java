package com.kavamala.JMSButler;

import java.util.Properties;

import com.tibco.tibjms.admin.RouteInfo;
import com.tibco.tibjms.admin.TibjmsAdmin;
import com.tibco.tibjms.admin.TibjmsAdminException;
import com.tibco.tibjms.admin.TibjmsAdminInvalidNameException;
import com.tibco.tibjms.admin.TibjmsAdminNameExistsException;

public final class EMSRouteManager {
	
	String cmdLine;
	TibjmsAdmin ta;
	RouteInfo ri = null;
	
	String routeName;
	String command;
	String description;
	Properties routeProperties;

	
	public EMSRouteManager(final String cmdLine, final TibjmsAdmin ta) throws Exception {
		super();
		this.cmdLine = cmdLine;
		this.ta = ta;
		EMSUtils utils = new EMSUtils();
		this.routeProperties = utils.propertiesInit(cmdLine);
		this.routeName = routeProperties.getProperty("__objName");
		this.command = routeProperties.getProperty("__command");
	}


	public void createOrUpdateRoute() throws Exception {
		p_createOrUpdateRoute();
	}


	public void deleteRoute() throws Exception {
		p_deleteRoute();
	}



	/**
	 * @throws TibjmsAdminException
	 * @throws TibjmsAdminInvalidNameException
	 * @throws TibjmsAdminNameExistsException
	 */
	private void p_createOrUpdateRoute() throws TibjmsAdminException,
			TibjmsAdminInvalidNameException {
		//debug
		System.out.println("EB: processing route "+ routeName );
		
		String url = routeProperties.getProperty(EMSConstants.URL);
		String zoneName = routeProperties.getProperty(EMSConstants.ZoneName, "");
		String zoneType = routeProperties.getProperty(EMSConstants.ZoneType, "");
	   	short zt = (zoneType.equals("1hop") ? RouteInfo.ZONE_TYPE_ONE_HOP : RouteInfo.ZONE_TYPE_MULTI_HOP);
		//String selector = routeProperties.getProperty(EMSConstants.Selector, null);
		
		ri=ta.getRoute(routeName);

	    if (ri == null) {   
	    	// FIXME add support for SSL properties
	    	// FIXME update inc/out selectors (routeselector)
	    	ri = new RouteInfo(routeName, url, null, zoneName, zt);
	    	ta.createRoute(ri);    	
	    } else {
	    	// FIXME update inc/out selectors (need to recreate the route?
	    	ri.setURL(routeProperties.getProperty(EMSConstants.URL));
	    	//ri.setSSLParams(arg0)
//	    	ri.setName(arg0)
	    } 
		
		//debug
		System.out.println("EB: route "+ routeName +" created/updated");
	}


	/**
	 * @throws TibjmsAdminException
	 * @throws TibjmsAdminInvalidNameException
	 */
	private void p_deleteRoute() throws TibjmsAdminException,
			TibjmsAdminInvalidNameException {
		//debug
		System.out.println("EB: processing route "+ routeName );
			   	
		ta.destroyRoute(routeName);
		
	   	//debug
		System.out.println("EB: route "+ routeName +" deleted");
	}
	
}
