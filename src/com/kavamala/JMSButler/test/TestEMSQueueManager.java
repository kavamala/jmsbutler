/**
 * 
 */
package com.kavamala.JMSButler.test;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.kavamala.JMSButler.JMSButler;

/**
 * @author Daniele Galluccio
 *
 */
public class TestEMSQueueManager implements JMSButlerTestInterface {
	
	static JMSButler jb = null;
	
	
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		//System.out.println("beforeClass");
		jb = new JMSButler(startupArgs);
	}

	/**
	 * 
	 */
	@AfterClass
	public static void tearDownAfterClass() {
		jb = null;
	}

	/**
	 * 
	 */
	@Before
	public void setUp() {
		//System.out.println("setup");
	}

	/**
	 * 
	 */
	@After
	public void tearDown() {
		//System.out.println("teardown");
	}

	/**
	 * Test method for {@link com.kavamala.JMSButler.EMSQueueManager#createOrUpdateQueue()}.
	 * @throws Exception 
	 */
	@Test
	public final void testCreateOrUpdateQueue() throws Exception {
		jb.parseCommand("create queue regressionTest.simple.1");

		//jb.parseCommand("create queue $$$$");
	}

	/**
	 * Test method for {@link com.kavamala.JMSButler.EMSQueueManager#deleteQueue()}.
	 * @throws Exception 
	 */
	@Test
	public final void testDeleteQueue() throws Exception {
		jb.parseCommand("delete queue regressionTest.simple.1");
		jb.parseCommand("delete queue regressionTest.simple");
	}


	@Test
	public final void testCreateDeleteAllQueues() throws Exception {
		jb.parseCommand("create queue regressionTest.simple.2");
		jb.parseCommand("create queue regressionTest.simple.3");
		jb.parseCommand("create queue regressionTest.survivor");
		jb.parseCommand("delete all queues regressionTest.simple.>");
		//jb.parseCommand("delete all queues");
		//jb.parseCommand("delete all queues ");
	}

	
	@Test
	public final void testShowQueues() throws Exception {
		jb.parseCommand("show queues");
	}	

}
