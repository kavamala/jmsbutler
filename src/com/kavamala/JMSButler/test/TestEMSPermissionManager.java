/**
 * 
 */
package com.kavamala.JMSButler.test;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.kavamala.JMSButler.JMSButler;
import com.tibco.tibjms.admin.ACLEntry;
import com.tibco.tibjms.admin.TibjmsAdmin;
import com.tibco.tibjms.admin.TibjmsAdminException;

/**
 * @author Administrator
 *
 */
public class TestEMSPermissionManager implements JMSButlerTestInterface {

	static JMSButler jb = null;
	static TibjmsAdmin ta = null ;
		
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		//System.out.println("beforeClass");
		jb = new JMSButler(startupArgs);
		ta = new TibjmsAdmin(serverUrl, userName, password);
	}
	
	

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		jb = null;
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.kavamala.JMSButler.EMSPermissionManager#setPermissions()}.
	 * @throws Exception 
	 */
	@Test
	public final void testQueueUserSetPermissions() throws Exception {


		//String cmdLine = "grant admin group=g1 view-all";
//			EMSPermissionManager epm = new EMSPermissionManager(cmdLine, ta);
//			epm.setPermissions();
		jb.parseCommand("create user daniele \"desc\" password=dani");
		showACLs();	
		jb.parseCommand("grant queue queue.sample user=daniele browse,send,receive");
		showACLs();		
		jb.parseCommand("revoke queue queue.sample user=daniele browse");
		showACLs();
	}

	/**
	 * @throws TibjmsAdminException
	 */
	private void showACLs() throws TibjmsAdminException {
		ACLEntry[] acls = ta.getACLEntries();
		for (int i = 0; i < acls.length; i++) {
			String princ = acls[i].getPrincipal().getName();
			String perm = acls[i].getPermissions().toString();
			System.out.println(princ + " : " + perm );
		}
	}



}
