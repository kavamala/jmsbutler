/**
 * 
 */
package com.kavamala.JMSButler.test;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.kavamala.JMSButler.JMSButler;

/**
 * @author Administrator
 *
 */
public class TestEMSTopicManager implements JMSButlerTestInterface {
	
	
	static JMSButler jb = null;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		jb = new JMSButler(startupArgs);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		jb = null;
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.kavamala.JMSButler.EMSTopicManager#createOrUpdateTopic()}.
	 */
	@Test
	public final void testCreateOrUpdateTopic() throws Exception {
		jb.parseCommand("create topic regressionTest.topic.1");
	}

	/**
	 * Test method for {@link com.kavamala.JMSButler.EMSTopicManager#deleteTopic()}.
	 */
	@Test
	public final void testDeleteTopic() throws Exception {
		jb.parseCommand("delete topic regressionTest.topic.1");
	}

	/**
	 * Test method for {@link com.kavamala.JMSButler.EMSTopicManager#deleteAllTopics()}.
	 */
	@Test
	public final void testDeleteAllTopics() throws Exception {
		jb.parseCommand("create topic regressionTest.topic.2");
		jb.parseCommand("create topic regressionTest.topic.3");
		jb.parseCommand("create topic regressionTest.survivor");
		jb.parseCommand("delete all topics regressionTest.topic.>");
		jb.parseCommand("delete all topics ");

		
	}
}
