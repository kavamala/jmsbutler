/**
 * 
 */
package com.kavamala.JMSButler.test;

import java.util.ArrayList;
import java.util.Properties;

import com.tibco.tibjms.admin.ACLEntry;
import com.tibco.tibjms.admin.Permissions;
import com.tibco.tibjms.admin.PrincipalInfo;
import com.tibco.tibjms.admin.QueueInfo;
import com.tibco.tibjms.admin.TibjmsAdmin;
import com.tibco.tibjms.admin.TibjmsAdminException;

/**
 * @author Administrator
 *
 */
public class PermissionGrantRevoke {

	
	
	String cmdLine;
	
	ACLEntry[] prmss = null;
	Properties permissionProperties;
	ArrayList<ACLEntry> ACLs = new ArrayList<ACLEntry>();
	
	/**
	 * @param args
	 * @throws TibjmsAdminException 
	 */
	public static void main(String[] args) throws TibjmsAdminException {
		String serverUrl="tcp://localhost:7223";
		String userName="admin";
		String password="";	
		TibjmsAdmin ta;
		
		ta = new TibjmsAdmin(serverUrl, userName, password);
		
		QueueInfo qi = ta.getQueue("queue.sample");
		PrincipalInfo pi = ta.getUser("daniele");
		Permissions p = new Permissions();
		p.setPermission(Permissions.BROWSE_PERMISSION, true);
		
		ACLEntry acl = new ACLEntry(qi,pi,p);
		
		ta.grant(acl);

		// working
//		p.setPermission(Permissions.BROWSE_PERMISSION, true);		
//		acl = new ACLEntry(qi,pi,p);
//		ta.revoke(acl);
		
		// not working
		p.setPermission(Permissions.BROWSE_PERMISSION, false);		
		acl = new ACLEntry(qi,pi,p);
		ta.grant(acl);
		
		System.out.println("done");
		
	}

}
