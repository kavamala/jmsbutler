/**
 * 
 */
package com.kavamala.JMSButler.test;

import java.util.Properties;

import junit.framework.TestCase;

import com.kavamala.JMSButler.EMSUtils;

/**
 * @author Administrator
 *
 */
public class TestEMSUtils extends TestCase {
	
	public void testParseSize() throws Exception{
	
		long base = 1000;
		long m = 1024;
		long a;

		a = EMSUtils.parseSize("" + base);
		assertEquals(base, a);

		a = EMSUtils.parseSize("" + base+"KB");
		assertEquals(base*m, a);
	
		a = EMSUtils.parseSize("" + base+"MB");
		assertEquals(base*m*m, a);

		a = EMSUtils.parseSize("" + base+"GB");
		assertEquals(base*m*m*m, a);

	} // end testParseSize
	
	
	
	public void testParseTime() throws Exception{

		long a;

		a = EMSUtils.parseTime("2000msec");
		assertEquals(2, a);

		a = EMSUtils.parseTime("23");
		assertEquals(23, a);
		a = EMSUtils.parseTime("23sec");
		assertEquals(23, a);
	
		a = EMSUtils.parseTime("10min");
		assertEquals(600, a);

		a = EMSUtils.parseTime("24hour");
		assertEquals(86400, a);

		a = EMSUtils.parseTime("1day");
		assertEquals(86400, a);
	} // end testParseSize	
	
	//TODO add other examples of command line to parse
	
	public void testSimpleParseCmdLine_topic() throws Exception {
		EMSUtils u = new EMSUtils();
		Properties p;
		String cmd1 = "create topic ebt2		flowControl=1000KB,maxmsgs=5,prefetch=2";
		p= u.simpleCommandParser(cmd1);		
		assertEquals("create", p.getProperty("__command"));
		assertEquals("topic", p.getProperty("__objType"));
		assertEquals("ebt2", p.getProperty("__objName"));
		assertEquals("5", p.getProperty("maxmsgs"));
		assertEquals("2", p.getProperty("prefetch"));
		assertEquals("1000KB", p.getProperty("flowcontrol"));
	} // end testSimpleParseCmdLine_topic

	public void testSimpleParseCmdLine_bridge() throws Exception {
		EMSUtils u = new EMSUtils();
		Properties p;
		String cmd1 = "create bridge source=Stype:Sdest_name target=Ttype:Tdest_name  selector=Selector";
		p= u.simpleCommandParser(cmd1);		
		assertEquals("Stype:Sdest_name", p.getProperty("source"));
		assertEquals("Ttype:Tdest_name", p.getProperty("target"));
		assertEquals("Selector", p.getProperty("selector"));		
	} // end testSimpleParseCmdLine_bridge

	public void testSimpleParseCmdLine_queue() throws Exception {
		EMSUtils u = new EMSUtils();
		Properties p;
		String cmd2 = "create queue sample.Q1 maxmsgs=1000,import=\"RV1,RV2\",overflowPolicy=discardOld,global,exclusive,flowControl=1000KB";
		p= u.simpleCommandParser(cmd2);		
		assertEquals("create", p.getProperty("__command"));
		assertEquals("queue", p.getProperty("__objType"));
		assertEquals("sample.Q1", p.getProperty("__objName"));
		assertEquals("1000", p.getProperty("maxmsgs"));
		assertEquals("discardOld", p.getProperty("overflowpolicy"));
		assertEquals("true", p.getProperty("global"));
		assertEquals("true", p.getProperty("exclusive"));
		assertEquals("1000KB", p.getProperty("flowcontrol"));
			
	} // end testSimpleParseCmdLine_queue
	
	
	public void testSimpleParseCmdLine_grant() throws Exception {
		EMSUtils u = new EMSUtils();
		Properties p;
		String cmd4 = "grant queue queue-name group=groupname receive,send,publish";
		p= u.simpleCommandParser(cmd4);		
		assertEquals("grant", p.getProperty("__command"));
		assertEquals("queue", p.getProperty("__objType"));
		assertEquals("queue-name", p.getProperty("__objName"));
		assertEquals("groupname", p.getProperty("group"));
		assertEquals("true", p.getProperty("receive"));	
		assertEquals("true", p.getProperty("send"));
		assertEquals("true", p.getProperty("publish"));
	} // end testSimpleParseCmdLine_x
	
	
	public void testSimpleParseCmdLine_route() throws Exception {
		EMSUtils u = new EMSUtils();
		Properties p;
		String cmd3 = "setprop route Canada outgoing_topic=\"orders\" selector=\"country=�Canada�\"";
		p= u.simpleCommandParser(cmd3);		
		assertEquals("setprop", p.getProperty("__command"));
		assertEquals("route", p.getProperty("__objType"));
		assertEquals("Canada", p.getProperty("__objName"));
		assertEquals("\"orders\"", p.getProperty("outgoing_topic"));
		assertEquals("\"country=�Canada�\"", p.getProperty("selector"));		
			
	} // end testSimpleParseCmdLine_x
	
	
	public void testhackCmdLine() throws Exception {
		EMSUtils u = new EMSUtils();
		
		String t1 = "a b c d=\"RV1,RV2\",f=dd,global,e=\"RV1,RV2\"";
		String t2 = u.normalizeCmdLine(t1);
		assertEquals("a b c d=\"RV1;RV2\",f=dd,global,e=\"RV1;RV2\"",t2);
	} // end testSimpleParseCmdLine	

	
} // end class
