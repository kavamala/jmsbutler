package com.kavamala.JMSButler.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;


/**
 * @author Daniele Galluccio
 *
 */

@RunWith(Suite.class)
@Suite.SuiteClasses({
	TestEMSUtils.class,
	TestJMSButler.class,
	TestEMSQueueManager.class,
	TestEMSTopicManager.class
//	TestEMSPermissionManager.class
})

public class JMSButlerTestSuite {

//	public static Test suite() {
//		TestSuite suite = new TestSuite("Test for com.kavamala.JMSButler.test");
//		//$JUnit-BEGIN$
//		suite.addTestSuite(TestEMSUtils.class);
//		suite.addTestSuite(TestJMSButler.class);
////		suite.addTestSuite(TestEMSQueueManager.class);
//		suite.addTestSuite(TestEMSTopicManager.class);
////		//suite.addTestSuite(TestEMSPermissionManager.class);
//		//$JUnit-END$
//		return suite;
//	}

}
