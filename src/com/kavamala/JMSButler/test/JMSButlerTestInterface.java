package com.kavamala.JMSButler.test;


public interface JMSButlerTestInterface {

	//static String serverUrl="ssl://localhost:7243";
	static String serverUrl="tcp://localhost:7222";
	static String userName="admin";
	static String password="";	
	static String[] startupArgs = {"-license","TestResources/Demo_license.key","-server", serverUrl,"-user", userName, "-password", password};
		
}