/**
 * 
 */
package com.kavamala.JMSButler.test;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.Enumeration;
import java.util.Properties;

import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.kavamala.JMSButler.EMSUtils;
import com.kavamala.JMSButler.GenUtils;
import com.kavamala.JMSButler.JMSButler;
import com.tibco.tibjms.admin.TibjmsAdmin;
import com.tibco.tibjms.admin.TibjmsAdminException;


/**
 * @author Daniele Galluccio
 *
 */
public class TestJMSButler implements JMSButlerTestInterface  {
	
//	String serverUrl="tcp://localhost:7222";
//	String userName="admin";
//	String password="";		
	static TibjmsAdmin ta = null ;
	
	
	@BeforeClass
    public static void oneTimeSetUp() throws TibjmsAdminException {
        // one-time initialization code   
		ta = new TibjmsAdmin(serverUrl, userName, password);

    }
	
	
	@Test
	public void testGenLicense() throws Exception {
		
		GenUtils desEncrypter = new GenUtils("MoadHJjYQGEX/AOHytu2QqA==");   		
		String usr = desEncrypter.encrypt("THIS IS A DEMO LICENCE");
		// dd-MM-yyyy
		String date = desEncrypter.encrypt("31-12-2015");
		String cmds = desEncrypter.encrypt(""+Integer.MAX_VALUE);
		System.out.println("genLicense:");
		System.out.println("usr : " +usr);
		System.out.println("date: " +date);
		System.out.println("cmds: " +cmds);
		
		
	} // end testObfuscate	
	
	
	/**
	 * Test method for {@link com.kavamala.JMSButler.JMSButler#checkLicense(String)}.
	 * @throws Exception
	 *
	 */
	@Test
	public void testLicence() throws Exception {
		JMSButler butler = new JMSButler();
		boolean valid =	butler.checkLicense("TestResources/EB_license.key");
		junit.framework.Assert.assertTrue(valid);
	} // end testLicence

	

	@Test
	public void testEmptyArgs() throws Exception {
		String args[] = null;
		JMSButler butler = new JMSButler(args);
		butler.printInfo();
	} // end testEmptyArgs
	
	@Test
	public void testObfuscate() throws Exception {
		JMSButler butler = new JMSButler();
		String ep = butler.obfuscate("test");
		Assert.assertEquals("#!p0uZykOTIkI=", ep);
	} // end testObfuscate

	@Test
	public void testGenObfuscate() throws Exception {
		String ep = JMSButler.genObfuscate("test");
		Assert.assertEquals("#!p0uZykOTIkI=", ep);
	} // end testObfuscate
	
	@Test
	public void testDeObfuscate() {
		try {
			JMSButler butler = new JMSButler();
			final Method[] m = butler.getClass().getDeclaredMethods();
			String ep = null;
			
			for (int i = 0; i < m.length; ++i) {
			  if (m[i].getName().equals("p_deObfuscate")) {
			    final Object params[] = {"#!p0uZykOTIkI="};
			    m[i].setAccessible(true);
			    Object ret = m[i].invoke(butler, params);
			    ep = ret.toString();
			    break;
			  }
			}
			Assert.assertEquals("test", ep);
		} catch (Exception e) {
			Assert.fail("unexpected error occured");
			e.printStackTrace();
		} 
	} // end testDeObfuscate

	@Test
	public void testLoadCredentials() throws Exception {
		JMSButler butler = new JMSButler();
		final Field fields[] = JMSButler.class.getDeclaredFields();
		
		butler.loadCredentials("TestResources/Credentials_unitTest.properties");
	    for (int i = 0; i < fields.length; ++i) {
	        fields[i].setAccessible(true);
	        //System.out.println(fields[i].getName() +": " + fields[i].get(butler));
	        if (fields[i].getName().equals("userName")) {
	        	System.out.println(fields[i].getName() +": " + fields[i].get(butler));
	        	Assert.assertEquals("user", fields[i].get(butler));
	        }
	        if (fields[i].getName().equals("serverUrl")) {
	        	System.out.println(fields[i].getName() +": " + fields[i].get(butler));
	        	Assert.assertEquals("url", fields[i].get(butler));
	        }
	        if (fields[i].getName().equals("password")) {
	        	System.out.println(fields[i].getName() +": " + fields[i].get(butler));
	        	Assert.assertEquals("pass", fields[i].get(butler));
	        }
	    }
	    
		butler.loadCredentials("TestResources/Credential_encrypted.properties");
	    for (int i = 0; i < fields.length; ++i) {
	        fields[i].setAccessible(true);
	        //System.out.println(fields[i].getName() +": " + fields[i].get(butler));
	        if (fields[i].getName().equals("userName")) {
	        	System.out.println(fields[i].getName() +": " + fields[i].get(butler));
	        	Assert.assertEquals("admin", fields[i].get(butler));
	        }
	        if (fields[i].getName().equals("serverUrl")) {
	        	System.out.println(fields[i].getName() +": " + fields[i].get(butler));
	        	Assert.assertEquals("tcp://localhost:7222", fields[i].get(butler));
	        }
	        if (fields[i].getName().equals("password")) {
	        	System.out.println(fields[i].getName() +": " + fields[i].get(butler));
	        	Assert.assertEquals("test", fields[i].get(butler));
	        }
	    }		
	} // end testObfuscate	

	@Test
	public void testNonDestructiveQueueCreation_NoRoute_NoBridge() throws Exception {
		String queueName = "queue.sample";
		String cmd1 = "create queue " + queueName + " prefetch=7,flowControl=1000KB,maxmsgs=50";
		p_testNonDestructiveQueueCreation_NoRoute_NoBridge(cmd1);
	
		String cmd2 = "create queue " + queueName + " maxmsgs=1000,overflowPolicy=discardOld,global,exclusive,flowControl=1000KB";
		p_testNonDestructiveQueueCreation_NoRoute_NoBridge(cmd2);
	} // end testNonDestructiveQueueCreation_NoRoute_NoBridge

	
	@Test
	public void testNonDestructiveQueueCreation_promoteDynamic() throws Exception {
		String queueName = "dynamic.queue";
		String cmd1 = "create queue " + queueName + " prefetch=7,flowControl=1000KB,maxmsgs=50";
		p_testNonDestructiveQueueCreation_NoRoute_NoBridge(cmd1);
	
		String cmd2 = "create queue " + queueName + " maxmsgs=1000,overflowPolicy=discardOld,global,exclusive,flowControl=1000KB";
		p_testNonDestructiveQueueCreation_NoRoute_NoBridge(cmd2);
	} // end testNonDestructiveQueueCreation_promoteDynamic
	
	
	
	private void p_testNonDestructiveQueueCreation_NoRoute_NoBridge(String cmd) throws Exception {
		
		EMSUtils utils = new EMSUtils();
		Properties p = utils.propertiesInit(cmd);
		String q = p.getProperty("__objName");
		
		JMSButler butler = new JMSButler();	
		// send sample msg to the queue
		String id = sendMessageToQueue(q);
        // check the msg is there
		Assert.assertTrue(queue_MessageExists(q, id));
        // execute the cmd
		butler.parseCommand(cmd);
        // check the msg is still there
		Assert.assertTrue(queue_MessageExists(q, id));
        
	} // end testNonDestructiveQueueCreation_NoRoute_NoBridge

	/**
	 * @param queueName
	 */
	private String sendMessageToQueue(String queueName) throws Exception {
            QueueConnectionFactory factory = new com.tibco.tibjms.TibjmsQueueConnectionFactory(serverUrl);
            QueueConnection connection = factory.createQueueConnection(userName,password);
            QueueSession session = connection.createQueueSession(false,javax.jms.Session.AUTO_ACKNOWLEDGE);
            javax.jms.Queue queue = session.createQueue(queueName);

            QueueSender sender = session.createSender(queue);

            javax.jms.TextMessage message = session.createTextMessage();
            String text = "jUnit msg: "+message.getJMSMessageID() + " @" + new Date().toString();
            System.err.println("--- Sending message:" + text);
            message.setText(text);
            sender.send(message);
            connection.close();
            
            
//            QueueConnectionFactory factory = new com.tibco.tibjms.TibjmsQueueConnectionFactory(serverUrl);
//            QueueConnection connection = factory.createQueueConnection(userName,password);
//            QueueSession session = connection.createQueueSession(false,javax.jms.Session.AUTO_ACKNOWLEDGE);
//            javax.jms.Queue queue = session.createQueue(queueName);
//            QueueSender sender = session.createSender(queue);

        return  message.getJMSMessageID();
	}
	
	
	private boolean queue_MessageExists(String queueName, String msgId) throws Exception {
        QueueConnectionFactory factory = new com.tibco.tibjms.TibjmsQueueConnectionFactory(serverUrl);
        QueueConnection connection = factory.createQueueConnection(userName,password);
        QueueSession session = connection.createQueueSession(false,javax.jms.Session.AUTO_ACKNOWLEDGE);

        javax.jms.Queue queue = session.createQueue(queueName);
        javax.jms.QueueBrowser browser = session.createBrowser(queue);

        Enumeration<?> msgs = browser.getEnumeration();

        int browseCount=0;
        boolean found = false;
        while(msgs.hasMoreElements())
        {
        	javax.jms.Message message = (javax.jms.Message)msgs.nextElement();
            if(message.getJMSMessageID().equals(msgId)) {
            	found = true;
            	System.err.println("--- Found message:" + msgId);
            }	
            browseCount++;
        }

        System.err.println("--- Found " + browseCount + " messages");
        
        session.close();
        connection.close();

       return found;
       // return true;
	}
	

	
}
