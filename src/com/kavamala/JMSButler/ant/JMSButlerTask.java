/**
 * 
 */
package com.kavamala.JMSButler.ant;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Arrays;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;

import com.kavamala.JMSButler.JMSButler;
import com.kavamala.JMSButler.Msg;
import com.tibco.tibjms.admin.TibjmsAdminException;


/**
 * @author Daniele Galluccio
 *
 */
public class JMSButlerTask  extends Task{

//	String[] params = new String[6];
	String[] params = {	"-cred","-", // cred
						"-license","-", 
						"-cmdFile","-",
						"-server","-",
						"-user","-",
						"-password","-"};
	
	String [] subArray;
	
	private String property;
	private String inLineCmds = null;
	File temp = null;
	boolean failOnError = true;
	
	
	@Override
	public void execute() throws BuildException {
		
		int status = -1;
		try {
			
			// Both inline and cmdfile present
			if (!params[5].equals("-") && inLineCmds != null) {
				mergeFiles(temp,params[5]);
			}
			
			
			if (params[1].equals("-")) {
				subArray = Arrays.copyOfRange(params, 2, 12);
			} else {
				subArray = Arrays.copyOfRange(params, 0, 6);
			}
			 
//			for (int i = 0; i < subArray.length; i++) {
//				System.out.println("sub:"+subArray[i]);	
//			}
			

			//JMSButler eb = new JMSButler(params);
			JMSButler eb = new JMSButler(subArray);
			eb.printInfo();

			if (inLineCmds == null && !eb.getCommandFile().equals("-")) { 
				eb.parseFile(eb.getCommandFile());
			} else {
				eb.parseFile(temp.getAbsolutePath());
			}			
			status = eb.getErrors().size();	

		} catch (TibjmsAdminException e) {
			System.out.println(Msg.get("EB.18")); //$NON-NLS-1$
			System.err.println(e.getLocalizedMessage());
			System.err.println(e.getCause());
		} catch (Exception e) {
			System.out.println(Msg.get("EB.19")); //$NON-NLS-1$
			System.err.println(e.getLocalizedMessage());
			System.err.println(e.getCause());
//			e.printStackTrace();
		} finally {
			//System.err.println("st:"+status);
			// FIXME improve errorhandling, maybe introduce failOnError
			if (property != null && !property.equals("")) { //$NON-NLS-1$
				getProject().setNewProperty(property, ""+status); //$NON-NLS-1$
				if(failOnError == true) {
					throw new BuildException(Msg.get("JB.ant.errorMsg")); //$NON-NLS-1$
				}
			} else if (status != 0) {
				if(failOnError == true) {
					throw new BuildException(Msg.get("JB.ant.errorMsg")); //$NON-NLS-1$
				} else {
					System.err.println(Msg.get("JB.ant.errorMsg"));
				}
			} 
		}
		
	} // end: execute

	public void addText(String inLine) {
		inLineCmds = getProject().replaceProperties(inLine);
		//inLineCmds = inLine;
		try {
		    temp = File.createTempFile("JmsButler-", ".tmp"); //$NON-NLS-1$
		    temp.deleteOnExit();
		    BufferedWriter out = new BufferedWriter(new FileWriter(temp));
		    out.write(inLineCmds);
		    out.close();
		} catch (Exception e) {
			System.err.println(e.getLocalizedMessage());
			System.err.println(e.getCause());
			throw new BuildException(Msg.get("JB.ant.fileCreate.errorMsg")); //$NON-NLS-1$
		}		
	} // end addText(String inLine) 

	public void setCredentials (String credFilePath) {
		params[0]="-cred"; //$NON-NLS-1$
		params[1]=credFilePath;
	}

	public void setLicense (String licenseFilePath) {
		params[2]="-license"; //$NON-NLS-1$
		params[3]=licenseFilePath;		
	}
	
	public void setCommandFile (String commandFilePath) {
		params[4]="-cmdFile"; //$NON-NLS-1$
		if (commandFilePath == null || commandFilePath.equals("")) {
			params[5]="-";
		} else {
			params[5]=commandFilePath;	
		}			
	}
	
	public void setUser (String user) {
		params[6]="-user";
		params[7]=user;
	}
	
	public void setServerUrl (String serverUrl) {
		params[8]="-server";
		params[9]=serverUrl;
	}
	
	public void setPassword (String password) {
		params[10]="-password";
		params[11]=password;
	}
	
	public void setProperty (String property) {
		this.property = property;
	}	
	
	public void setFailOnError (boolean failOnError) {
		this.failOnError = failOnError;
	}	
	
	private void mergeFiles(File tmpFile, String cmdFile) throws Exception {

	    File inputFile = new File(cmdFile);
	    File outputFile = tmpFile;

	    FileReader in = new FileReader(inputFile);
	    FileWriter out = new FileWriter(outputFile);
	    
	    int c;
	    while ((c = in.read()) != -1) {
	      out.write(c);
	    }
	    out.write(inLineCmds);
	    
	    in.close();
	    out.close();		
	}
}
