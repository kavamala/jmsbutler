package com.kavamala.JMSButler;

import java.util.Properties;

import com.tibco.tibjms.admin.GroupInfo;
import com.tibco.tibjms.admin.TibjmsAdmin;
import com.tibco.tibjms.admin.TibjmsAdminException;
import com.tibco.tibjms.admin.TibjmsAdminInvalidNameException;
import com.tibco.tibjms.admin.TibjmsAdminNameExistsException;

/**
 * @author Daniele Galluccio
 *
 */
public class EMSGroupManager {
	
	String cmdLine;
	TibjmsAdmin ta;
	GroupInfo gi = null;
	
	String groupName;
	String command;
	String description;
	Properties groupProperties;

	
	public EMSGroupManager(final String cmdLine, final TibjmsAdmin ta) throws Exception {
		super();
		this.cmdLine = cmdLine;
		this.ta = ta;
		EMSUtils utils = new EMSUtils();
		groupProperties = utils.propertiesInit(cmdLine);
		groupName = groupProperties.getProperty("__objName");
		command = groupProperties.getProperty("__command");
		//TODO document ~ replacement
		description = groupProperties.getProperty("__description").replaceAll("~", " ");

	}


	public void createOrUpdateGroup() throws Exception {
		p_createOrUpdateGroup();
	}


	public void deleteGroup() throws Exception {
		p_deleteGroup();
	}


	/**
	 * @throws TibjmsAdminException
	 * @throws TibjmsAdminInvalidNameException
	 * @throws TibjmsAdminNameExistsException
	 */
	private void p_createOrUpdateGroup() throws TibjmsAdminException,
			TibjmsAdminInvalidNameException, TibjmsAdminNameExistsException {
		//debug
		//System.out.println("EB: processing group "+ groupName );
		
	   	gi=ta.getGroup(groupName);
	    if (gi == null) {   
	    	//System.out.println("-- queue does not exists");
	    	gi = new GroupInfo(groupName);
	    	gi.setDescription(description);
	    	ta.createGroup(gi);
	    	
	    } else {
	    	//System.out.println("found topic " + groupName);
	    	gi.setDescription(description);
	    	ta.updateGroup(gi);
	    } 
		
		//debug
		System.out.println("EB: group "+ groupName +" created/updated");
	}


	/**
	 * @throws TibjmsAdminException
	 * @throws TibjmsAdminInvalidNameException
	 */
	private void p_deleteGroup() throws TibjmsAdminException,
			TibjmsAdminInvalidNameException {
		//debug
		//System.out.println("EB: processing group "+ groupName );
		
	   	ta.destroyGroup(groupName);
	   	
	   	//debug
		System.out.println("EB: group "+ groupName +" deleted");
	}


	public void deleteAllGroups() throws Exception {
		p_deleteAllGroups();
	}


	private void p_deleteAllGroups() throws TibjmsAdminInvalidNameException, TibjmsAdminException {
		GroupInfo gis[] = ta.getGroups();
		for (int i = 0; i < gis.length; i++) {
			ta.destroyGroup(gis[i].getName());
		}
	}


}
