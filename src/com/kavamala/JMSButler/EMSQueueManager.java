package com.kavamala.JMSButler;

import java.util.Iterator;
import java.util.Properties;

import com.tibco.tibjms.admin.DestinationInfo;
import com.tibco.tibjms.admin.QueueInfo;
import com.tibco.tibjms.admin.TibjmsAdmin;
import com.tibco.tibjms.admin.TibjmsAdminException;
import com.tibco.tibjms.admin.TibjmsAdminInvalidNameException;

/**
 * 
 * @author Daniele Galluccio
 *
 * v1.1 Fixed check in queue creation for promoting dynamic queues
 */
public final  class EMSQueueManager {

	String cmdLine;
	TibjmsAdmin ta;
	QueueInfo qi = null;
	
	String queueName;
	String command;
	String description;
	Properties queueProperties = new Properties();

	
	public EMSQueueManager(String cmdLine, TibjmsAdmin ta) throws Exception {
		super();
		this.cmdLine = cmdLine;
		this.ta = ta;
		p_init(cmdLine);	
	}

	public void createOrUpdateQueue() throws Exception {
		p_basicCreateQueue();   
        p_updateQueue();
		//debug
		System.out.println(Msg.get("1.0")+ queueName +Msg.get("1.1")); //$NON-NLS-1$ //$NON-NLS-2$
	} // end createOrUpdateQueue

	
	public void deleteQueue() throws Exception {
		//debug
		System.out.println(Msg.get("1.2")+ queueName ); //$NON-NLS-1$
		
	   	ta.destroyQueue(queueName);
	   	
	   	//debug
		System.out.println(Msg.get("1.3")+ queueName +Msg.get("1.4")); //$NON-NLS-1$ //$NON-NLS-2$
	} // end deleteQueue

	/**
	 * @param cmdLine
	 * @throws Exception
	 */
	private void p_init(final String cmdLine) throws Exception {
		EMSUtils utils = new EMSUtils();
		queueProperties = utils.simpleCommandParser(cmdLine);
		queueName = queueProperties.getProperty(Msg.get("1.5")); //$NON-NLS-1$
		command = queueProperties.getProperty(Msg.get("1.6")); //$NON-NLS-1$
		for (Iterator<?> i = queueProperties.keySet().iterator(); i.hasNext();) {
			String key = (String) i.next();
			if (key.startsWith(Msg.get("1.7")) && key.endsWith(Msg.get("1.8"))) { //$NON-NLS-1$ //$NON-NLS-2$
				description = key.substring(1, key.length()-1);
			}
		}
	}

	/**
	 * @throws TibjmsAdminException
	 * @throws TibjmsAdminInvalidNameException
	 */
	private void p_basicCreateQueue() throws TibjmsAdminException,
			TibjmsAdminInvalidNameException {
		//debug
		System.out.println(Msg.get("1.9")+ queueName ); //$NON-NLS-1$
		
	   	qi=ta.getQueue(queueName);
	    if (qi == null || !qi.isStatic()) {   
	    	//System.out.println("-- queue does not exists");
	    	qi = new QueueInfo(queueName);
	    	ta.createQueue(qi);
	    	//System.out.println("queue: "+ queueName +" created");
	    } else {
	    	//System.out.println("found queue " + queueName);
	    }
	} // end p_BasicCreateQueue

	/**
	 * @throws TibjmsAdminInvalidNameException
	 * @throws Exception
	 * @throws TibjmsAdminException
	 */
	@SuppressWarnings("deprecation")
	private void p_updateQueue() throws TibjmsAdminInvalidNameException,
			Exception, TibjmsAdminException {
		//
        // Assigning queue properties
        //
        // TODO @deprecated method
        qi.setDescription(description);

        String[] Imports = queueProperties.getProperty(EMSConstants.Import, Msg.get("1.10")).split(Msg.get("1.11")); //$NON-NLS-1$ //$NON-NLS-2$
        for (int i = 0; !Imports[i].equals(Msg.get("1.12")) && i < Imports.length; i++) { //$NON-NLS-1$
        	qi.addImportTransport(Imports[i]);
        	//qi.removeImportTransport(arg0)
		}
		
		qi.setExclusive(Boolean.parseBoolean(queueProperties.getProperty(EMSConstants.Exclusive, Msg.get("1.13")))); //$NON-NLS-1$
		String Expiration = queueProperties.getProperty(EMSConstants.Expiration, Msg.get("1.14")); //$NON-NLS-1$
		qi.setExpiryOverride(EMSUtils.parseTime(Expiration));
		qi.setFailsafe(Boolean.parseBoolean(queueProperties.getProperty(EMSConstants.Failsafe, Msg.get("1.15")))); //$NON-NLS-1$
		
		// FIXME implement following special case:
		// If you specify the flowControl property without a value, the target maximum is set to 256KB. 
		String FlowControl = queueProperties.getProperty(EMSConstants.FlowControlMaxBytes, Msg.get("1.16")); //$NON-NLS-1$
		qi.setFlowControlMaxBytes(EMSUtils.parseSize(FlowControl));
		
		qi.setGlobal(Boolean.parseBoolean(queueProperties.getProperty(EMSConstants.Global, Msg.get("1.17")))); //$NON-NLS-1$
		String MaxBytes = queueProperties.getProperty(EMSConstants.MaxBytes, Msg.get("1.18")); //$NON-NLS-1$
		qi.setMaxBytes(EMSUtils.parseSize(MaxBytes));
		qi.setMaxMsgs(Long.parseLong(queueProperties.getProperty(EMSConstants.MaxMsgs, Msg.get("1.19"))));	
		qi.setMaxRedelivery(Integer.parseInt(queueProperties.getProperty(EMSConstants.MaxRedelivery, Msg.get("1.20"))));
		
		String trace = queueProperties.getProperty(EMSConstants.Trace, null);
		if (trace != null) {
			if (trace.equalsIgnoreCase(Msg.get("1.21"))) { //$NON-NLS-1$
				qi.setMsgTrace(DestinationInfo.MSG_TRACE_BASIC);
			} else if (trace.equalsIgnoreCase(Msg.get("1.22"))) { //$NON-NLS-1$
				qi.setMsgTrace(DestinationInfo.MSG_TRACE_DETAIL);
			} else {
				throw new Exception(Msg.get("1.23")); //$NON-NLS-1$
			}
		} else {
			qi.setMsgTrace(DestinationInfo.MSG_TRACE_NONE);
		}
		String OverflowPolicy = queueProperties.getProperty(EMSConstants.OverflowPolicy,Msg.get("1.24")); //$NON-NLS-1$
		if (OverflowPolicy.compareToIgnoreCase(Msg.get("1.25")) == 0) { //$NON-NLS-1$
			qi.setOverflowPolicy(DestinationInfo.OVERFLOW_DISCARD_OLD);
		} else if (OverflowPolicy.compareToIgnoreCase(Msg.get("1.26")) == 0) { //$NON-NLS-1$
			qi.setOverflowPolicy(DestinationInfo.OVERFLOW_REJECT_INCOMING);
		} else {
			qi.setOverflowPolicy(DestinationInfo.OVERFLOW_DEFAULT);
		}
		qi.setPrefetch(Integer.parseInt(queueProperties.getProperty(EMSConstants.Prefetch, Msg.get("1.27")+DestinationInfo.PREFETCH_DEFAULT)));	 //$NON-NLS-1$
		qi.setSecure(Boolean.parseBoolean(queueProperties.getProperty(EMSConstants.Secure, Msg.get("1.28")))); //$NON-NLS-1$
		qi.setSenderName(Boolean.parseBoolean(queueProperties.getProperty(EMSConstants.SenderName, Msg.get("1.29")))); //$NON-NLS-1$
		qi.setSenderNameEnforced(Boolean.parseBoolean(queueProperties.getProperty(EMSConstants.SenderNameEnforced, Msg.get("1.30")))); //$NON-NLS-1$
		if (!queueProperties.getProperty(EMSConstants.Store,Msg.get("1.31")).equals(Msg.get("1.32"))) { //$NON-NLS-1$ //$NON-NLS-2$
			qi.setStore(queueProperties.getProperty(EMSConstants.Store, Msg.get("1.33")));	 //$NON-NLS-1$
		}
	  
		ta.updateQueue(qi);
		
	} // end p_updateQueue

	public void deleteAllQueues() throws Exception    {
		try {
			p_deleteAllQueues();
		} catch (TibjmsAdminInvalidNameException e) {
			throw e;
		} catch (TibjmsAdminException e) {
			// ignore TibjmsAdminException for this command
		}
	}

	private void p_deleteAllQueues() throws TibjmsAdminInvalidNameException, TibjmsAdminException {
		String qf[] = cmdLine.split("queues");
		if(qf.length == 2 && !qf[1].trim().equals("")) {
			ta.destroyQueues(qf[1].trim());
		} else {
			ta.destroyQueues(">");
		}
	}
	
}
