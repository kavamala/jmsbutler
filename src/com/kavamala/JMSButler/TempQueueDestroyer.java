package com.kavamala.JMSButler;

import com.tibco.tibjms.admin.QueueInfo;
import com.tibco.tibjms.admin.TibjmsAdmin;

public class TempQueueDestroyer
{

    public TempQueueDestroyer(String args[])
    {
        serverUrl = null;
        userName = null;
        password = null;
        queuePattern = "$TMP$.>";
        noReceiver = 0;
        parseArgs(args);
        QueueInfo qi[] = (QueueInfo[])null;
        try
        {
            TibjmsAdmin ta = new TibjmsAdmin(serverUrl, userName, password);
            qi = ta.getQueues(queuePattern);
            System.out.println((new StringBuilder("Number of temporary queues: ")).append(qi.length).toString());
            for(int i = 0; i < qi.length; i++)
                if(qi[i].getReceiverCount() == 0)
                {
                    noReceiver++;
                    System.out.println((new StringBuilder("Deleting queue..")).append(qi[i].getName()).toString());
                    ta.destroyQueue(qi[i].getName());
                    System.out.println((new StringBuilder("Deleted queue..")).append(qi[i].getName()).toString());
                } else
                {
                    System.out.println((new StringBuilder("Queue: ")).append(qi[i].getName()).append(" has ").append(qi[i].getReceiverCount()).append(" receivers").toString());
                }

            System.out.println((new StringBuilder("Number of temporary queues without receiver: ")).append(noReceiver).toString());
        }
        catch(Exception err)
        {
            err.printStackTrace();
        }
    }

    public static void main(String args[])
    {
        new TempQueueDestroyer(args);
    }

    void usage()
    {
        System.err.println("\nUsage: java TempQueueDestoryer [options]");
        System.err.println("");
        System.err.println("  where options are:");
        System.err.println("");
        System.err.println("  -server   <server-url>  - EMS server URL");
        System.err.println("  -user     <user-name>   - admin user name");
        System.err.println("  -password <password>    - admin password");
        System.exit(0);
    }

    void parseArgs(String args[])
    {
        int i = 0;
        if(args.length == 0)
            usage();
        while(i < args.length) 
            if(args[i].compareTo("-server") == 0)
            {
                if(i + 1 >= args.length)
                    usage();
                serverUrl = args[i + 1];
                i += 2;
            } else
            if(args[i].compareTo("-user") == 0)
            {
                if(i + 1 >= args.length)
                    usage();
                userName = args[i + 1];
                i += 2;
            } else
            if(args[i].compareTo("-password") == 0)
            {
                if(i + 1 >= args.length)
                    usage();
                password = args[i + 1];
                i += 2;
            } else
            {
                System.err.println((new StringBuilder("Unrecognized parameter: ")).append(args[i]).toString());
                usage();
            }
    }

    String serverUrl;
    String userName;
    String password;
    String queuePattern;
    int noReceiver;
}
