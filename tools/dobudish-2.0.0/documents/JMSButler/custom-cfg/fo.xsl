﻿<?xml version="1.0" encoding="UTF-8"?>

<!-- This file is part of DobuDish                                           -->

<!-- DobuDish is free software; you can redistribute it and/or modify        -->
<!-- it under the terms of the GNU General Public License as published by    -->
<!-- the Free Software Foundation; either version 2 of the License, or       -->
<!-- (at your option) any later version.                                     -->

<!-- DobuDish is distributed in the hope that it will be useful,             -->
<!-- but WITHOUT ANY WARRANTY; without even the implied warranty of          -->
<!-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           -->
<!-- GNU General Public License for more details.                            -->

<!-- You should have received a copy of the GNU General Public License       -->
<!-- along with DobuDish; if not, write to the Free Software                 -->
<!-- Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA -->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                version='1.0'>

  <xsl:import href="../../../system/custom-xsl/fo-book.xsl"/>
  <xsl:import href="common.xsl"/>
  
  
    <xsl:param name="title.font.family">Georgia</xsl:param>
    <xsl:param name="body.font.family">Verdana</xsl:param>
    
    
    <!-- Custom Revision History template -->
    <!-- Do not display the "Revision History" title -->
    <xsl:template match="revhistory" mode="titlepage.mode">
      <xsl:variable name="explicit.table.width">
        <xsl:call-template name="dbfo-attribute">
          <xsl:with-param name="pis"
                          select="processing-instruction('dbfo')"/>
          <xsl:with-param name="attribute" select="'table-width'"/>
        </xsl:call-template>
      </xsl:variable>
    
      <xsl:variable name="table.width">
        <xsl:choose>
          <xsl:when test="$explicit.table.width != ''">
            <xsl:value-of select="$explicit.table.width"/>
          </xsl:when>
          <xsl:when test="$default.table.width = ''">
            <xsl:text>100%</xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="$default.table.width"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:variable>
   
 
      <fo:block xsl:use-attribute-sets="revhistory.title.properties">
            <xsl:call-template name="gentext">
              <xsl:with-param name="key" select="'RevHistory'"/>
            </xsl:call-template>
      </fo:block>
      
      <fo:table table-layout="fixed" width="{$table.width}" xsl:use-attribute-sets="revhistory.table.properties">
        <fo:table-column column-number="1" column-width="proportional-column-width(2)"/>
        <fo:table-column column-number="2" column-width="proportional-column-width(1)"/>
        <fo:table-column column-number="3" column-width="proportional-column-width(2)"/>
        <fo:table-column column-number="4" column-width="proportional-column-width(5)"/>
        <fo:table-body start-indent="0pt" end-indent="0pt" space-before="8pt">
          <xsl:apply-templates mode="titlepage.mode"/>
        </fo:table-body>
      </fo:table>
    </xsl:template>

    <xsl:template match="para[@role='mycomment']">
        <fo:inline
            background-color="#88ff88"
            >
            <xsl:apply-templates/>
        </fo:inline>
    </xsl:template>

</xsl:stylesheet>
